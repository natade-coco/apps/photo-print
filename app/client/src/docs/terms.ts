export const terms = `<!doctype html>
<html>
<head>
<meta charset='UTF-8'><meta name='viewport' content='width=device-width initial-scale=1'>
<title>フォトプリントサービス利用規約</title><link href='https://fonts.loli.net/css?family=Open+Sans:400italic,700italic,700,400&subset=latin,latin-ext' rel='stylesheet' type='text/css' /><style type='text/css'>html {overflow-x: initial !important;}:root { --bg-color: #ffffff; --text-color: #333333; --select-text-bg-color: #B5D6FC; --select-text-font-color: auto; --monospace: "Lucida Console",Consolas,"Courier",monospace; }
html { font-size: 14px; background-color: var(--bg-color); color: var(--text-color); font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; -webkit-font-smoothing: antialiased; }
body { margin: 0px; padding: 0px; height: auto; bottom: 0px; top: 0px; left: 0px; right: 0px; font-size: 1rem; line-height: 1.42857143; overflow-x: hidden; background-image: inherit; background-size: inherit; background-attachment: inherit; background-origin: inherit; background-clip: inherit; background-color: inherit; tab-size: 4; background-position: inherit inherit; background-repeat: inherit inherit; }
iframe { margin: auto; }
a.url { word-break: break-all; }
a:active, a:hover { outline: 0px; }
.in-text-selection, ::selection { text-shadow: none; background: var(--select-text-bg-color); color: var(--select-text-font-color); }
#write { margin: 0px auto; height: auto; w
    idth: inherit; word-break: normal; word-wrap: break-word; position: relative; white-space: normal; overflow-x: visible; padding-top: 40px; }
#write.first-line-indent p { text-indent: 2em; }
#write.first-line-indent li p, #write.first-line-indent p * { text-indent: 0px; }
#write.first-line-indent li { margin-left: 2em; }
.for-image #write { padding-left: 8px; padding-right: 8px; }
body.typora-export { padding-left: 30px; padding-right: 30px; }
.typora-export .footnote-line, .typora-export li, .typora-export p { white-space: pre-wrap; }
.typora-export .task-list-item input { pointer-events: none; }
@media screen and (max-width: 500px) { 
  body.typora-export { padding-left: 0px; padding-right: 0px; }
  #write { padding-left: 20px; padding-right: 20px; }
  .CodeMirror-sizer { margin-left: 0px !important; }
  .CodeMirror-gutters { display: none !important; }
}
#write li > figure:last-child { margin-bottom: 0.5rem; }
#write ol, #write ul { position: relative; }
img { max-width: 100%; vertical-align: middle; }
button, input, select, textarea { color: inherit; font-family: inherit; font-size: inherit; font-style: inherit; font-variant-caps: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; }
input[type="checkbox"], input[type="radio"] { line-height: normal; padding: 0px; }
*, ::after, ::before { box-sizing: border-box; }
#write h1, #write h2, #write h3, #write h4, #write h5, #write h6, #write p, #write pre { width: inherit; }
#write h1, #write h2, #write h3, #write h4, #write h5, #write h6, #write p { position: relative; }
p { line-height: inherit; }
h1, h2, h3, h4, h5, h6 { break-after: avoid-page; break-inside: avoid; orphans: 4; }
p { orphans: 4; }
h1 { font-size: 2rem; }
h2 { font-size: 1.8rem; }
h3 { font-size: 1.6rem; }
h4 { font-size: 1.4rem; }
h5 { font-size: 1.2rem; }
h6 { font-size: 1rem; }
.md-math-block, .md-rawblock, h1, h2, h3, h4, h5, h6, p { margin-top: 1rem; margin-bottom: 1rem; }
.hidden { display: none; }
.md-blockmeta { color: rgb(204, 204, 204); font-weight: 700; font-style: italic; }
a { cursor: pointer; }
sup.md-footnote { padding: 2px 4px; background-color: rgba(238, 238, 238, 0.701961); color: rgb(85, 85, 85); border-top-left-radius: 4px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; cursor: pointer; }
sup.md-footnote a, sup.md-footnote a:hover { color: inherit; text-transform: inherit; text-decoration: inherit; }
#write input[type="checkbox"] { cursor: pointer; width: inherit; height: inherit; }
figure { overflow-x: auto; margin: 1.2em 0px; max-width: calc(100% + 16px); padding: 0px; }
figure > table { margin: 0px; }
tr { break-inside: avoid; break-after: auto; }
thead { display: table-header-group; }
table { border-collapse: collapse; border-spacing: 0px; width: 100%; overflow: auto; break-inside: auto; text-align: left; }
table.md-table td { min-width: 32px; }
.CodeMirror-gutters { border-right-width: 0px; background-color: inherit; }
.CodeMirror-linenumber { }
.CodeMirror { text-align: left; }
.CodeMirror-placeholder { opacity: 0.3; }
.CodeMirror pre { padding: 0px 4px; }
.CodeMirror-lines { padding: 0px; }
div.hr:focus { cursor: none; }
#write pre { white-space: pre-wrap; }
#write.fences-no-line-wrapping pre { white-space: pre; }
#write pre.ty-contain-cm { white-space: normal; }
.CodeMirror-gutters { margin-right: 4px; }
.md-fences { font-size: 0.9rem; display: block; break-inside: avoid; text-align: left; overflow: visible; white-space: pre; background-image: inherit; background-size: inherit; background-attachment: inherit; background-origin: inherit; background-clip: inherit; background-color: inherit; position: relative !important; background-position: inherit inherit; background-repeat: inherit inherit; }
.md-diagram-panel { width: 100%; margin-top: 10px; text-align: center; padding-top: 0px; padding-bottom: 8px; overflow-x: auto; }
#write .md-fences.mock-cm { white-space: pre-wrap; }
.md-fences.md-fences-with-lineno { padding-left: 0px; }
#write.fences-no-line-wrapping .md-fences.mock-cm { white-space: pre; overflow-x: auto; }
.md-fences.mock-cm.md-fences-with-lineno { padding-left: 8px; }
.CodeMirror-line, twitterwidget { break-inside: avoid; }
.footnotes { opacity: 0.8; font-size: 0.9rem; margin-top: 1em; margin-bottom: 1em; }
.footnotes + .footnotes { margin-top: 0px; }
.md-reset { margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: top; text-decoration: none; text-shadow: none; float: none; position: static; width: auto; height: auto; white-space: nowrap; cursor: inherit; line-height: normal; font-weight: 400; text-align: left; box-sizing: content-box; direction: ltr; background-position: 0px 0px; background-repeat: initial initial; }
li div { padding-top: 0px; }
blockquote { margin: 1rem 0px; }
li .mathjax-block, li p { margin: 0.5rem 0px; }
li { margin: 0px; position: relative; }
blockquote > :last-child { margin-bottom: 0px; }
blockquote > :first-child, li > :first-child { margin-top: 0px; }
.footnotes-area { color: rgb(136, 136, 136); margin-top: 0.714rem; padding-bottom: 0.143rem; white-space: normal; }
#write .footnote-line { white-space: pre-wrap; }
@media print { 
  body, html { border: 1px solid transparent; height: 99%; break-after: avoid-page; break-before: avoid-page; font-variant-ligatures: no-common-ligatures; }
  #write { margin-top: 0px; padding-top: 0px; border-color: transparent !important; }
  .typora-export * { -webkit-print-color-adjust: exact; }
  html.blink-to-pdf { font-size: 13px; }
  .typora-export #write { break-after: avoid-page; }
  .typora-export #write::after { height: 0px; }
  .is-mac table { break-inside: avoid; }
}
.footnote-line { margin-top: 0.714em; font-size: 0.7em; }
a img, img a { cursor: pointer; }
pre.md-meta-block { font-size: 0.8rem; min-height: 0.8rem; white-space: pre-wrap; background-color: rgb(204, 204, 204); display: block; overflow-x: hidden; background-position: initial initial; background-repeat: initial initial; }
p > .md-image:only-child:not(.md-img-error) img, p > img:only-child { display: block; margin: auto; }
#write.first-line-indent p > .md-image:only-child:not(.md-img-error) img { left: -2em; position: relative; }
p > .md-image:only-child { display: inline-block; width: 100%; }
#write .MathJax_Display { margin: 0.8em 0px 0px; }
.md-math-block { width: 100%; }
.md-math-block:not(:empty)::after { display: none; }
[contenteditable="true"]:active, [contenteditable="true"]:focus, [contenteditable="false"]:active, [contenteditable="false"]:focus { outline: 0px; box-shadow: none; }
.md-task-list-item { position: relative; list-style-type: none; }
.task-list-item.md-task-list-item { padding-left: 0px; }
.md-task-list-item > input { position: absolute; top: 0px; left: 0px; margin-left: -1.2em; margin-top: calc(1em - 10px); border: none; }
.math { font-size: 1rem; }
.md-toc { min-height: 3.58rem; position: relative; font-size: 0.9rem; border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px; }
.md-toc-content { position: relative; margin-left: 0px; }
.md-toc-content::after, .md-toc::after { display: none; }
.md-toc-item { display: block; color: rgb(65, 131, 196); }
.md-toc-item a { text-decoration: none; }
.md-toc-inner:hover { text-decoration: underline; }
.md-toc-inner { display: inline-block; cursor: pointer; }
.md-toc-h1 .md-toc-inner { margin-left: 0px; font-weight: 700; }
.md-toc-h2 .md-toc-inner { margin-left: 2em; }
.md-toc-h3 .md-toc-inner { margin-left: 4em; }
.md-toc-h4 .md-toc-inner { margin-left: 6em; }
.md-toc-h5 .md-toc-inner { margin-left: 8em; }
.md-toc-h6 .md-toc-inner { margin-left: 10em; }
@media screen and (max-width: 48em) { 
  .md-toc-h3 .md-toc-inner { margin-left: 3.5em; }
  .md-toc-h4 .md-toc-inner { margin-left: 5em; }
  .md-toc-h5 .md-toc-inner { margin-left: 6.5em; }
  .md-toc-h6 .md-toc-inner { margin-left: 8em; }
}
a.md-toc-inner { font-size: inherit; font-style: inherit; font-weight: inherit; line-height: inherit; }
.footnote-line a:not(.reversefootnote) { color: inherit; }
.md-attr { display: none; }
.md-fn-count::after { content: "."; }
code, pre, samp, tt { font-family: var(--monospace); }
kbd { margin: 0px 0.1em; padding: 0.1em 0.6em; font-size: 0.8em; color: rgb(36, 39, 41); background-color: rgb(255, 255, 255); border: 1px solid rgb(173, 179, 185); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; box-shadow: rgba(12, 13, 14, 0.2) 0px 1px 0px, rgb(255, 255, 255) 0px 0px 0px 2px inset; white-space: nowrap; vertical-align: middle; background-position: initial initial; background-repeat: initial initial; }
.md-comment { color: rgb(162, 127, 3); opacity: 0.8; font-family: var(--monospace); }
code { text-align: left; }
a.md-print-anchor { white-space: pre !important; border: none !important; display: inline-block !important; position: absolute !important; width: 1px !important; right: 0px !important; outline: 0px !important; text-shadow: initial !important; background-position: 0px 0px !important; background-repeat: initial initial !important; }
.md-inline-math .MathJax_SVG .noError { display: none !important; }
.html-for-mac .inline-math-svg .MathJax_SVG { vertical-align: 0.2px; }
.md-math-block .MathJax_SVG_Display { text-align: center; margin: 0px; position: relative; text-indent: 0px; max-width: none; max-height: none; min-height: 0px; min-width: 100%; width: auto; overflow-y: hidden; display: block !important; }
.MathJax_SVG_Display, .md-inline-math .MathJax_SVG_Display { width: auto; margin: inherit; display: inline-block !important; }
.MathJax_SVG .MJX-monospace { font-family: var(--monospace); }
.MathJax_SVG .MJX-sans-serif { font-family: sans-serif; }
.MathJax_SVG { display: inline; font-style: normal; font-weight: 400; line-height: normal; zoom: 90%; text-indent: 0px; text-align: left; text-transform: none; letter-spacing: normal; word-spacing: normal; word-wrap: normal; white-space: nowrap; float: none; direction: ltr; max-width: none; max-height: none; min-width: 0px; min-height: 0px; border: 0px; padding: 0px; margin: 0px; }
.MathJax_SVG * { transition: none; }
.MathJax_SVG_Display svg { vertical-align: middle !important; margin-bottom: 0px !important; margin-top: 0px !important; }
.os-windows.monocolor-emoji .md-emoji { font-family: "Segoe UI Symbol", sans-serif; }
.md-diagram-panel > svg { max-width: 100%; }
[lang="flow"] svg, [lang="mermaid"] svg { max-width: 100%; height: auto; }
[lang="mermaid"] .node text { font-size: 1rem; }
table tr th { border-bottom-width: 0px; }
video { max-width: 100%; display: block; margin: 0px auto; }
iframe { max-width: 100%; width: 100%; border: none; }
.highlight td, .highlight tr { border: 0px; }
svg[id^="mermaidChart"] { line-height: 1em; }
mark { background-color: rgb(255, 255, 0); color: rgb(0, 0, 0); background-position: initial initial; background-repeat: initial initial; }
.md-html-inline .md-plain, .md-html-inline strong, mark .md-inline-math, mark strong { color: inherit; }
mark .md-meta { color: rgb(0, 0, 0); opacity: 0.3 !important; }
@media print { 
  .typora-export h1, .typora-export h2, .typora-export h3, .typora-export h4, .typora-export h5, .typora-export h6 { break-inside: avoid; }
}


:root {
    --side-bar-bg-color: #fafafa;
    --control-text-color: #777;
}

@include-when-export url(https://fonts.loli.net/css?family=Open+Sans:400italic,700italic,700,400&subset=latin,latin-ext);

/* open-sans-regular - latin-ext_latin */
  /* open-sans-italic - latin-ext_latin */
    /* open-sans-700 - latin-ext_latin */
    /* open-sans-700italic - latin-ext_latin */
  html {
    font-size: 16px;
}

body {
    font-family: "Open Sans","Clear Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    color: rgb(51, 51, 51);
    line-height: 1.6;
}

#write {
    max-width: 860px;
  	margin: 0 auto;
  	padding: 30px;
    padding-bottom: 100px;
}

@media only screen and (min-width: 1400px) {
	#write {
		max-width: 1024px;
	}
}

@media only screen and (min-width: 1800px) {
	#write {
		max-width: 1200px;
	}
}

#write > ul:first-child,
#write > ol:first-child{
    margin-top: 30px;
}

a {
    color: #4183C4;
}
h1,
h2,
h3,
h4,
h5,
h6 {
    position: relative;
    margin-top: 1rem;
    margin-bottom: 1rem;
    font-weight: bold;
    line-height: 1.4;
    cursor: text;
}
h1:hover a.anchor,
h2:hover a.anchor,
h3:hover a.anchor,
h4:hover a.anchor,
h5:hover a.anchor,
h6:hover a.anchor {
    text-decoration: none;
}
h1 tt,
h1 code {
    font-size: inherit;
}
h2 tt,
h2 code {
    font-size: inherit;
}
h3 tt,
h3 code {
    font-size: inherit;
}
h4 tt,
h4 code {
    font-size: inherit;
}
h5 tt,
h5 code {
    font-size: inherit;
}
h6 tt,
h6 code {
    font-size: inherit;
}
h1 {
    font-size: 2.25em;
    line-height: 1.2;
    border-bottom: 1px solid #eee;
}
h2 {
    font-size: 1.75em;
    line-height: 1.225;
    border-bottom: 1px solid #eee;
}

/*@media print {
    .typora-export h1,
    .typora-export h2 {
        border-bottom: none;
        padding-bottom: initial;
    }

    .typora-export h1::after,
    .typora-export h2::after {
        content: "";
        display: block;
        height: 100px;
        margin-top: -96px;
        border-top: 1px solid #eee;
    }
}*/

h3 {
    font-size: 1.5em;
    line-height: 1.43;
}
h4 {
    font-size: 1.25em;
}
h5 {
    font-size: 1em;
}
h6 {
   font-size: 1em;
    color: #777;
}
p,
blockquote,
ul,
ol,
dl,
table{
    margin: 0.8em 0;
}
li>ol,
li>ul {
    margin: 0 0;
}
hr {
    height: 2px;
    padding: 0;
    margin: 16px 0;
    background-color: #e7e7e7;
    border: 0 none;
    overflow: hidden;
    box-sizing: content-box;
}

li p.first {
    display: inline-block;
}
ul,
ol {
    padding-left: 30px;
}
ul:first-child,
ol:first-child {
    margin-top: 0;
}
ul:last-child,
ol:last-child {
    margin-bottom: 0;
}
blockquote {
    border-left: 4px solid #dfe2e5;
    padding: 0 15px;
    color: #777777;
}
blockquote blockquote {
    padding-right: 0;
}
table {
    padding: 0;
    word-break: initial;
}
table tr {
    border-top: 1px solid #dfe2e5;
    margin: 0;
    padding: 0;
}
table tr:nth-child(2n),
thead {
    background-color: #f8f8f8;
}
table tr th {
    font-weight: bold;
    border: 1px solid #dfe2e5;
    border-bottom: 0;
    margin: 0;
    padding: 6px 13px;
}
table tr td {
    border: 1px solid #dfe2e5;
    margin: 0;
    padding: 6px 13px;
}
table tr th:first-child,
table tr td:first-child {
    margin-top: 0;
}
table tr th:last-child,
table tr td:last-child {
    margin-bottom: 0;
}

.CodeMirror-lines {
    padding-left: 4px;
}

.code-tooltip {
    box-shadow: 0 1px 1px 0 rgba(0,28,36,.3);
    border-top: 1px solid #eef2f2;
}

.md-fences,
code,
tt {
    border: 1px solid #e7eaed;
    background-color: #f8f8f8;
    border-radius: 3px;
    padding: 0;
    padding: 2px 4px 0px 4px;
    font-size: 0.9em;
}

code {
    background-color: #f3f4f4;
    padding: 0 2px 0 2px;
}

.md-fences {
    margin-bottom: 15px;
    margin-top: 15px;
    padding-top: 8px;
    padding-bottom: 6px;
}


.md-task-list-item > input {
  margin-left: -1.3em;
}

@media print {
    html {
        font-size: 13px;
    }
    table,
    pre {
        page-break-inside: avoid;
    }
    pre {
        word-wrap: break-word;
    }
}

.md-fences {
	background-color: #f8f8f8;
}
#write pre.md-meta-block {
	padding: 1rem;
    font-size: 85%;
    line-height: 1.45;
    background-color: #f7f7f7;
    border: 0;
    border-radius: 3px;
    color: #777777;
    margin-top: 0 !important;
}

.mathjax-block>.code-tooltip {
	bottom: .375rem;
}

.md-mathjax-midline {
    background: #fafafa;
}

#write>h3.md-focus:before{
	left: -1.5625rem;
	top: .375rem;
}
#write>h4.md-focus:before{
	left: -1.5625rem;
	top: .285714286rem;
}
#write>h5.md-focus:before{
	left: -1.5625rem;
	top: .285714286rem;
}
#write>h6.md-focus:before{
	left: -1.5625rem;
	top: .285714286rem;
}
.md-image>.md-meta {
    /*border: 1px solid #ddd;*/
    border-radius: 3px;
    padding: 2px 0px 0px 4px;
    font-size: 0.9em;
    color: inherit;
}

.md-tag {
    color: #a7a7a7;
    opacity: 1;
}

.md-toc { 
    margin-top:20px;
    padding-bottom:20px;
}

.sidebar-tabs {
    border-bottom: none;
}

#typora-quick-open {
    border: 1px solid #ddd;
    background-color: #f8f8f8;
}

#typora-quick-open-item {
    background-color: #FAFAFA;
    border-color: #FEFEFE #e5e5e5 #e5e5e5 #eee;
    border-style: solid;
    border-width: 1px;
}

/** focus mode */
.on-focus-mode blockquote {
    border-left-color: rgba(85, 85, 85, 0.12);
}

header, .context-menu, .megamenu-content, footer{
    font-family: "Segoe UI", "Arial", sans-serif;
}

.file-node-content:hover .file-node-icon,
.file-node-content:hover .file-node-open-state{
    visibility: visible;
}

.mac-seamless-mode #typora-sidebar {
    background-color: #fafafa;
    background-color: var(--side-bar-bg-color);
}

.md-lang {
    color: #b4654d;
}

.html-for-mac .context-menu {
    --item-hover-bg-color: #E6F0FE;
}

#md-notification .btn {
    border: 0;
}

.dropdown-menu .divider {
    border-color: #e5e5e5;
}

.ty-preferences .window-content {
    background-color: #fafafa;
}

.ty-preferences .nav-group-item.active {
    color: white;
    background: #999;
}

 .typora-export li, .typora-export p, .typora-export,  .footnote-line {white-space: normal;} 
</style>
</head>
<body class='typora-export'>
<div id='write'  class=''><h1><a name="natadecoco-フォトプリントサービス-利用規約" class="md-header-anchor"></a><span>natadeCOCO フォトプリントサービス 利用規約</span></h1><p><span>本利用規約（以下「本規約」といいます。）は、株式会社スーパーソフトウエア（以下「当社」といいます。）が提供する、「natadeCOCO フォトプリントサービス」（以下「本サービス」といいます。）の利用条件を定めるものです。本サービスの利用に際しては、本規約の全文をお読みいただいたうえで、本規約に同意いただく必要があります。</span></p><h2><a name="第1条定義）" class="md-header-anchor"></a><span>第1条（定義）</span></h2><p><span>本規約において使用する以下の用語は、各々以下に定める意味を有するものとします。</span></p><figure><table><thead><tr><th style='text-align:left;' ><span>用語</span></th><th style='text-align:left;' ><span>意味</span></th></tr></thead><tbody><tr><td style='text-align:left;' ><span>サービス利用契約</span></td><td style='text-align:left;' ><span>本規約を契約条件として当社とユーザーの間で締結される、本サービスの利用契約</span></td></tr><tr><td style='text-align:left;' ><span>知的財産権</span></td><td style='text-align:left;' ><span>著作権、特許権、実用新案権、意匠権、商標権その他の知的財産権（それらの権利を取得し、またはそれらの権利につき登録等を出願する権利を含みます。）</span></td></tr><tr><td style='text-align:left;' ><span>natadeCOCOアプリ</span></td><td style='text-align:left;' ><span>当社が提供するスマートフォンアプリ「natadeCOCO」</span></td></tr><tr><td style='text-align:left;' ><span>ユーザー</span></td><td style='text-align:left;' ><span>本サービスを利用するnatadeCOCOアプリのユーザー</span></td></tr><tr><td style='text-align:left;' ><span>本アプリ</span></td><td style='text-align:left;' ><span>当社が提供する、ユーザーが本サービスを利用するためにnatadeCOCOアプリ上で操作するウェブアプリ</span></td></tr><tr><td style='text-align:left;' ><span>フレーム画像</span></td><td style='text-align:left;' ><span>当社が本サービス上で提供するフレームの画像</span></td></tr><tr><td style='text-align:left;' ><span>フォトプリンター</span></td><td style='text-align:left;' ><span>当社が本サービスの提供エリア内に設置する、写真を印刷するための機器（写真用紙、インクを含みます。）</span></td></tr><tr><td style='text-align:left;' ><span>当社スタッフ</span></td><td style='text-align:left;' ><span>本サービスで印刷された写真を所定の提供場所でユーザーに手渡しする当社の人員</span></td></tr></tbody></table></figure><h2><a name="第2条本サービスの概要）" class="md-header-anchor"></a><span>第2条（本サービスの概要）</span></h2><ol start='' ><li><span>本サービスは、当社がnatadeCOCOアプリを通じて本サービスの提供エリア内に居るユーザー向けに提供するサービスであり、本サービスの提供エリア外に居るユーザーは本サービスを利用できません。</span></li><li><span>本サービスは、ユーザーが、本アプリ上で撮影またはモバイル端末内から選択した画像データに、選択したフレーム画像を重ね合わせた写真を取得できるサービスです。</span></li><li><span>本サービスでは、ユーザーから受け付けた印刷データが順次フォトプリンターで印刷され、当該写真を所定の提供場所において当社スタッフがユーザーに提供します。</span></li><li><span>提供する写真のサイズはL判（89x127mm）、横向きです。</span></li></ol><h2><a name="第3条適用）" class="md-header-anchor"></a><span>第3条（適用）</span></h2><ol start='' ><li><span>本規約は、本サービスの提供条件及び本サービスの利用に関する当社とユーザーとの間の権利義務関係を定めることを目的とし、ユーザーと当社との間の本サービスの利用に関わる一切の関係に適用されます。</span></li><li><span>当社が本アプリ上で掲載する本サービス利用に関するルールは、本規約の一部を構成するものとします。</span></li><li><span>本規約の内容と、前項のルールその他の本規約外における本サービスの説明等とが異なる場合は、本規約の規定が優先して適用されるものとします。</span></li></ol><h2><a name="第4条利用開始）" class="md-header-anchor"></a><span>第4条（利用開始）</span></h2><ol start='' ><li><p><span>本サービスの利用開始を希望する者（以下「利用希望者」といいます。）は、予めモバイル端末にnatadeCOCOアプリをインストールし、natadeCOCOアプリ内で当社が定める方法により本アプリにアクセスし、本規約を遵守することに同意することで本サービスの利用を開始することができます。</span></p></li><li><p><span>ユーザーが本サービスの利用を開始した時点で、サービス利用契約がユーザーと当社の間に成立するものとします。</span></p></li><li><p><span>利用希望者は、以下の各号のいずれかの事由に該当する場合、本サービスの利用を開始してはなりません。</span></p><ol start='' ><li><span>未成年者、成年被後見人、被保佐人または被補助人のいずれかであり、法定代理人、後見人、保佐人または補助人の同意等を得ていない場合</span></li><li><span>反社会的勢力等（暴力団、暴力団員、右翼団体、反社会的勢力、その他これに準ずる者を意味します。以下同じ。）である、または資金提供その他を通じて反社会的勢力等の維持、運営もしくは経営に協力もしくは関与する等反社会的勢力等との何らかの交流もしくは関与を行っている場合</span></li><li><span>居住する国または本サービスを利用する国の法律に基づき、本サービスを利用することを禁止されている場合</span></li><li><span>過去当社との契約に違反した者またはその関係者である場合</span></li></ol></li></ol><h2><a name="第5条アカウントの管理）" class="md-header-anchor"></a><span>第5条（アカウントの管理）</span></h2><ol start='' ><li><span>本サービスでは、ユーザーを一意に識別するためのアカウントとして、natadeCOCOアプリのアカウントを利用します。</span></li><li><span>ユーザーは、natadeCOCOアプリの利用規約に則り、アカウントを適切に管理及び保管するものとします。</span></li></ol><h2><a name="第6条料金及び利用条件）" class="md-header-anchor"></a><span>第6条（料金及び利用条件）</span></h2><ol start='' ><li><span>ユーザーは、本サービスを無料で利用できます。</span></li><li><span>本サービスには、ユーザーのアカウントごとに印刷可能な回数に制限が課せられており、ユーザーは、この回数を超えて印刷を行うことはできません。当該回数は本アプリ内にて表記されます。</span></li><li><span>前項の回数はユーザーが本アプリ上で印刷データを送信するごとにカウントされ、日付が変わるタイミングでリセットされます。但し、当社は、当社の都合により当該回数及び増減の条件を変更する場合があり、ユーザーはこれを予め承諾するものとします。</span></li><li><span>ユーザーは、印刷データ送信後、同日内に当社の定める受け取り場所で写真を取得するものとします。ユーザーが同日内に取得を行わなかった場合、当社は翌日以降に当該写真をユーザーに提供することなく破棄できるものとし、ユーザーはこれを予め承諾するものとします。</span></li><li><span>本サービスの性質上、写真の内容が当社スタッフ及び第三者に見られる可能性があることを、ユーザーは予め承諾するものとします。</span></li><li><span>本サービスの性質上、印刷データの受け付けから写真の提供までに時間を要する場合があることを、ユーザーは予め承諾するものとします。</span></li></ol><h2><a name="第7条禁止事項）" class="md-header-anchor"></a><span>第7条（禁止事項）</span></h2><p><span>ユーザーは、本サービスの利用にあたり、以下の各号のいずれかに該当する行為または該当すると当社が判断する行為をしてはなりません。</span></p><ol start='' ><li><p><span>法令に違反する行為または犯罪行為に関連する行為</span></p></li><li><p><span>公序良俗に反する行為</span></p></li><li><p><span>当社、本サービスの他の利用者またはその他の第三者の知的財産権、肖像権、プライバシーの権利、名誉、その他の権利または利益を侵害する行為</span></p></li><li><p><span>本サービスを通じ、以下に該当し、または該当すると当社が判断する情報（印刷する画像データを含みます。）を当社に送信すること</span></p><ul><li><span>過度に暴力的または残虐な表現を含む情報</span></li><li><span>当社、本サービスの他の利用者またはその他の第三者の名誉または信用を毀損する表現を含む情報</span></li><li><span>過度にわいせつな表現を含む情報</span></li><li><span>児童ポルノ・児童虐待に相当する表現を含む情報</span></li><li><span>差別を助長する表現を含む情報</span></li><li><span>自殺、自傷行為を助長する表現を含む情報</span></li><li><span>薬物の不適切な利用を助長する表現を含む情報</span></li><li><span>反社会的な表現を含む情報</span></li><li><span>他人に不快感を与える表現を含む情報</span></li></ul></li><li><p><span>複数のアカウントを利用し、本サービスの印刷回数制限を超えて印刷を行う行為</span></p></li><li><p><span>本サービスで取得した写真を販売する行為</span></p></li><li><p><span>フォトプリンターを本サービス外で利用する行為</span></p></li><li><p><span>本サービスのネットワークまたはシステム等に過度な負荷をかける行為</span></p></li><li><p><span>当社が提供するソフトウェアその他のシステムに対するリバースエンジニアリングその他の解析行為</span></p></li><li><p><span>本サービスの運営を妨害するおそれのある行為</span></p></li><li><p><span>当社、本サービスの他の利用者またはその他の第三者に不利益、損害、不快感を与える行為</span></p></li><li><p><span>反社会的勢力等への利益供与</span></p></li><li><p><span>その他、当社が不適切と判断する行為</span></p></li></ol><h2><a name="第8条本サービスの停止等）" class="md-header-anchor"></a><span>第8条（本サービスの停止等）</span></h2><p><span>当社は、以下のいずれかに該当する場合には、ユーザーに事前に通知することなく、本サービスの全部または一部の提供を停止または中断することができるものとします。</span></p><ol start='' ><li><span>本サービスに係るフォトプリンター及びコンピューター・システムの点検または保守作業を緊急に行う場合</span></li><li><span>フォトプリンター、コンピューター、通信回線等の障害、誤操作、過度なアクセスの集中、不正アクセス、ハッキング等により本サービスの運営ができなくなった場合</span></li><li><span>地震、落雷、火災、風水害、停電、天災地変などの不可抗力により本サービスの運営ができなくなった場合</span></li><li><span>その他、当社が停止または中断を必要と判断した場合</span></li></ol><h2><a name="第9条権利帰属）" class="md-header-anchor"></a><span>第9条（権利帰属）</span></h2><p><span>本サービスに関する知的財産権は全て当社または当社にライセンスを許諾している者に帰属しており、本規約に基づく本サービスの利用許諾は、本サービスに関する当社または当社にライセンスを許諾している者の知的財産権の使用許諾を意味するものではありません。</span></p><h2><a name="第10条利用停止等）" class="md-header-anchor"></a><span>第10条（利用停止等）</span></h2><ol start='' ><li><p><span>当社は、ユーザーが、以下の各号のいずれかの事由に該当する場合は、事前に通知または催告することなく、当該ユーザーについて本サービスの利用を一時的または恒久的に停止することができます。</span></p><ol start='' ><li><span>本規約のいずれかの条項に違反した場合</span></li><li><span>6ヶ月以上本サービスの利用がない場合</span></li><li><span>当社からの問い合わせその他の回答を求める連絡に対して30日間以上応答がない場合</span></li><li><span>第4条（利用開始）第3項各号に該当する場合</span></li><li><span>その他、当社が本サービスの利用を適当でないと判断した場合</span></li></ol></li><li><p><span>前項各号のいずれかの事由に該当した場合、かつ当社に対して負っている債務が有る場合、ユーザーは、当社に対して負っている債務の一切について当然に期限の利益を失い、直ちに当社に対して全ての債務の支払を行わなければなりません。</span></p></li></ol><h2><a name="第11条利用終了）" class="md-header-anchor"></a><span>第11条（利用終了）</span></h2><ol start='' ><li><span>ユーザーは、natadeCOCOアプリ内での操作等により本アプリを閉じることで、いつでも本サービスの利用を終了することができます。</span></li><li><span>誤って本アプリを閉じた場合その他理由の如何を問わず、ユーザーが本サービスを利用する権利を失った場合、ユーザーは、本サービスにおいて蓄積した情報を利用することができなくなることを、予め承諾するものとします。</span></li><li><span>利用終了にあたり、当社に対して負っている債務が有る場合は、ユーザーは、当社に対して負っている債務の一切について当然に期限の利益を失い、直ちに当社に対して全ての債務の支払を行わなければなりません。</span></li><li><span>利用終了後の利用者情報の取扱いについては、第15条（利用者情報の取扱い）の規定に従うものとします。</span></li></ol><h2><a name="第12条本サービスの内容の変更終了）" class="md-header-anchor"></a><span>第12条（本サービスの内容の変更、終了）</span></h2><p><span>当社は、当社の都合により、本サービスの内容を変更し、または提供を終了することができます。</span></p><h2><a name="第13条保証の否認及び免責）" class="md-header-anchor"></a><span>第13条（保証の否認及び免責）</span></h2><ol start='' ><li><span>当社は、本サービスがユーザーの特定の目的に適合すること、期待する機能・商品的価値・正確性・有用性を有すること、ユーザーによる本サービスの利用がユーザーに適用のある法令または業界団体の内部規則等に適合すること、継続的に利用できること、及び不具合が生じないことについて、明示または黙示を問わず何ら保証するものではありません。</span></li><li><span>当社は、ユーザーが本サービスを利用することで必ず写真を取得できることを、明示または黙示を問わず何ら保証するものではありません。</span></li><li><span>当社は、本サービスで提供する写真がユーザーの期待する品質・商品的価値・正確性・有用性を有することについて、明示または黙示を問わず何ら保証するものではありません。</span></li><li><span>当社は、本サービスで印刷された写真が、当社の過失、第三者による盗難、またはその他の理由により第三者の手に渡ったことにより生じるあらゆる損害について、一切の責任を負わないものとします。</span></li><li><span>当社は、本サービスに関してユーザーが被った損害につき、過去6ヶ月間にユーザーが当社に支払った対価の金額を超えて賠償する責任を負わないものとし、また、付随的損害、間接損害、特別損害、将来の損害及び逸失利益にかかる損害については、賠償する責任を負わないものとします。</span></li><li><span>本サービスに関連してユーザーと他のユーザーまたはその他の第三者との間において生じた取引、連絡、紛争等については、ユーザーが自己の責任によって解決するものとします。</span></li></ol><h2><a name="第14条秘密保持）" class="md-header-anchor"></a><span>第14条（秘密保持）</span></h2><p><span>ユーザーは、本サービスに関連して当社がユーザーに対して秘密に取扱うことを求めて開示した非公知の情報について、当社の事前の書面による承諾がある場合を除き、秘密に取扱うものとします。</span></p><h2><a name="第15条利用者情報の取扱い）" class="md-header-anchor"></a><span>第15条（利用者情報の取扱い）</span></h2><ol start='' ><li><span>当社によるユーザーの利用者情報の取扱いについては、別途当社プライバシーポリシーの定めによるものとし、ユーザーはこのプライバシーポリシーに従って当社がユーザーの利用者情報を取扱うことについて同意するものとします。</span></li><li><span>当社は、ユーザーが当社に提供した情報、データ等を、個人を特定できない形での統計的な情報として、当社の裁量で、利用及び公開することができるものとし、ユーザーはこれに異議を唱えないものとします。</span></li></ol><h2><a name="第16条本規約等の変更）" class="md-header-anchor"></a><span>第16条（本規約等の変更）</span></h2><p><span>当社は、当社が必要と認めた場合は、本規約を変更できるものとします。</span></p><h2><a name="第17条連絡通知）" class="md-header-anchor"></a><span>第17条（連絡／通知）</span></h2><ol start='' ><li><span>本サービスに関する問い合わせその他ユーザーから当社に対する連絡または通知、及び本規約の変更に関する通知その他当社からユーザーに対する連絡または通知は、当社の定める方法で行うものとします。</span></li><li><span>当社がユーザーが本サービスを利用するモバイル端末に連絡または通知を行った場合、ユーザーは当該連絡または通知を受領したものとみなします。</span></li></ol><h2><a name="第18条サービス利用契約上の地位の譲渡等）" class="md-header-anchor"></a><span>第18条（サービス利用契約上の地位の譲渡等）</span></h2><ol start='' ><li><span>ユーザーは、当社の書面による事前の承諾なく、利用契約上の地位または本規約に基づく権利もしくは義務につき、第三者に対し、譲渡、移転、担保設定、その他の処分をすることはできません。</span></li><li><span>当社は本サービスにかかる事業を他社に譲渡した場合には、当該事業譲渡に伴い利用契約上の地位、本規約に基づく権利及び義務並びにユーザーの登録事項その他の顧客情報を当該事業譲渡の譲受人に譲渡することができるものとし、ユーザーは、かかる譲渡につき本項において予め同意したものとします。なお、本項に定める事業譲渡には、通常の事業譲渡のみならず、会社分割その他事業が移転するあらゆる場合を含むものとします。</span></li></ol><h2><a name="第19条分離可能性）" class="md-header-anchor"></a><span>第19条（分離可能性）</span></h2><p><span>本規約のいずれかの条項またはその一部が、消費者契約法その他の法令等により無効または執行不能と判断された場合であっても、本規約の残りの規定及び一部が無効または執行不能と判断された規定の残りの部分は、継続して完全に効力を有するものとします。</span></p><h2><a name="第20条準拠法及び管轄裁判所）" class="md-header-anchor"></a><span>第20条（準拠法及び管轄裁判所）</span></h2><ol start='' ><li><span>本規約及びサービス利用契約の準拠法は日本法とします。</span></li><li><span>本規約またはサービス利用契約に起因し、または関連する一切の紛争については、当社の本社所在地を管轄する裁判所を第一審の専属的合意管轄裁判所とします。</span></li></ol><p><span>\</span>
<span>2021年3月15日 制定</span></p></div>
</body>
</html>`
