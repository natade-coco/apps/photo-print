import { combineReducers } from '@reduxjs/toolkit'
import AppReducer from '../slices/app'
import PrintdocReducer from '../slices/printdoc'
import SettingReducer from '../slices/setting'
import PhotoframeReducer from '../slices/photoframe'

const rootReducer = combineReducers({
  app: AppReducer,
  printdoc: PrintdocReducer,
  setting: SettingReducer,
  photoframe: PhotoframeReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
