import { createAsyncThunk, createSlice, createSelector } from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Setting } from '../entities/setting'
import SettingService from '../services/server/setting'

const settingService = new SettingService()

export interface State extends Partial<Setting> {}

const initialState: State = {}

// Selectors

const settingISelector = (state: RootState) => state.setting

export const printLimitSelector = createSelector(settingISelector, (state) => state.printLimit)
export const hasNumberSelector = createSelector(settingISelector, (state) => state.hasNumber)
export const printAmountSelector = createSelector(settingISelector, (state) => state.printAmount)
export const isPaidSelector = createSelector(settingISelector, (state) => state.isPaid)
export const stripeAccountSelector = createSelector(settingISelector, (state) => state.stripeAccount)

// Action
export const getSetting = createAsyncThunk(
  'setting/get',
  async (): Promise<Setting> => {
    try {
      const result = await settingService.getSettings()
      return result
    } catch (err) {
      throw err
    }
  }
)

const slice = createSlice({
  name: 'setting',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getSetting.fulfilled, (state, { payload }) => {
      Object.assign(state, { ...payload })
    })
  }
})

export default slice.reducer
