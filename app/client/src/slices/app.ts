import {
  createSlice,
  PayloadAction,
  createSelector,
  createAsyncThunk,
  isPending,
  isRejected,
  isFulfilled,
  createAction
} from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Session } from '../entities/app'
import PocketService from '../services/pocket'
import AppService from '../services/server/app'
import PrintcountService from '../services/server/printcount'

const appService = new AppService()
const pocket = new PocketService()
const printcountService = new PrintcountService()

export interface State {
  session: Session
  pendingActions: string[]
  error: string | null
  showError: boolean
  errorContent?: { title: string; message: string }
}

const initialState: State = {
  pendingActions: [],
  error: null,
  session: null,
  showError: false
}

// Selectors

const appISelector = (state: RootState) => state.app
export const isLoadingSelector = createSelector(appISelector, (state) => state.pendingActions.length > 0)
export const sessionIdSelector = createSelector(appISelector, (state) => state.session?.user?.id)
export const envSelector = createSelector(appISelector, (state) => state.session?.env)
export const errorSelector = createSelector(appISelector, (state) => state.error)
export const showErrorSelector = createSelector(appISelector, (state) => state.showError)
export const errorContentSelector = createSelector(appISelector, (state) => state.errorContent)
export const printCountSelector = createSelector(appISelector, (state) => state.session?.user?.printCount || 0)
export const userSelector = createSelector(appISelector, (state) => state.session?.user?.id)
export const printIdSelector = createSelector(appISelector, (state: State) => state.session?.user?.printId)
export const printTotalSelector = createSelector(appISelector, (state) => state.session?.user?.printTotal)
// Action

export const appStart = createAsyncThunk(
  'app/start',
  async (): Promise<Session> => {
    try {
      const jwt = await pocket.requestSignJWT()
      const session = await appService.getSession(jwt)
      return session
    } catch (err) {
      throw err
    }
  }
)
export const openError = createAction<{ title: string; message: string }>('app/openError')
export const closeError = createAction('app/closeError')
export const incrementPrintCnt = createAsyncThunk(
  'app/incrementPrintCnt',
  async (payload: { id: number; cnt: number; total: number }) => {
    const { id, cnt, total } = payload
    const result = await printcountService.updatePrintcount(id, cnt, total)
    return cnt
  }
)

// Slice

const getOriginalActionType = (action: PayloadAction<any>): string => action.type.slice(0, action.type.lastIndexOf('/'))

const startLoading = (state: State, action: PayloadAction<any>) => {
  const set = new Set(state.pendingActions)
  set.add(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
}

const loadingSuccess = (state: State, action: PayloadAction<any>) => {
  const set = new Set(state.pendingActions)
  set.delete(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
  state.error = null
}

const loadingFailed = (state: State, action: PayloadAction<any, string, any, any>) => {
  const set = new Set(state.pendingActions)
  set.delete(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
  state.error = action.error?.message
}
const slice = createSlice({
  name: 'app',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(appStart.fulfilled, (state, { payload }) => {
        state.session = payload
      })
      .addCase(openError, (state, { payload }) => {
        state.showError = true
        state.errorContent = payload
      })
      .addCase(closeError, (state) => {
        state.showError = false
      })
      .addCase(incrementPrintCnt.fulfilled, (state, { payload }) => {
        state.session.user.printCount = payload
      })
      .addMatcher(isPending, startLoading)
      .addMatcher(isFulfilled, loadingSuccess)
      .addMatcher(isRejected, loadingFailed)
  }
})

export default slice.reducer
