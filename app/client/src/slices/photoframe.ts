import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Photoframe } from '../entities/photoframe'
import PhotoframeService from '../services/server/photoframe'

const photoframeService = new PhotoframeService()

const entityAdapter = createEntityAdapter<Photoframe>()

// Selectors
const photoframeISelector = (state: RootState) => state.photoframe
export const photoframeSelector = entityAdapter.getSelectors(photoframeISelector)

// Thunks
export const getPhotoframes = createAsyncThunk(
  'photoframe/getList',
  async (): Promise<Photoframe[]> => {
    try {
      const result = await photoframeService.getPhotoframes()
      return result
    } catch (err) {
      throw err
    }
  }
)

const slice = createSlice({
  name: 'photoframe',
  initialState: entityAdapter.getInitialState(),
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getPhotoframes.fulfilled, entityAdapter.setAll)
  }
})

export default slice.reducer
