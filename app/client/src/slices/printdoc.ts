import { createAction, createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Printdoc, JobState, PrintdocParam } from '../entities/printdoc'
import FileService from '../services/server/file'
import PrintdocService from '../services/server/printdoc'

const printdocService = new PrintdocService()
const fileService = new FileService()

const entityAdapter = createEntityAdapter<Printdoc>({
  sortComparer: (a, b) => (a.reference > b.reference ? -1 : 1)
})

// Selectors
const printdocISelector = (state: RootState) => state.printdoc

export const printdocSelector = entityAdapter.getSelectors(printdocISelector)

// Thunks
export const getPrintdocs = createAsyncThunk(
  'printdoc/getList',
  async (did: string): Promise<Printdoc[]> => {
    try {
      const printdocs = await printdocService.getPrintdocs(did)
      return printdocs
    } catch (err) {
      throw err
    }
  }
)

export const createPrintdoc = createAsyncThunk(
  'printdoc/create',
  async (payload: {
    doc: any
    did: string
    reference: number
    frameId: number
    isVertically: boolean
    status?: JobState
    isPaid?: boolean
  }): Promise<Printdoc> => {
    try {
      const { doc, did, reference, frameId, isVertically, status } = payload
      const image = await fileService.createFile(doc)
      const result = await printdocService.createPrintdoc(image, did, reference, frameId, isVertically, status)
      return result
    } catch (err) {
      throw err
    }
  }
)

export const updatePrintdocRead = createAsyncThunk(
  'printdoc/updateRead',
  async (ids: number[]): Promise<Printdoc[]> => {
    try {
      const printdoc = await printdocService.updateReadPrintdocs(ids)
      return printdoc
    } catch (err) {
      throw err
    }
  }
)
export const updatePrintdocState = createAction<{ id: number; status: JobState }>('printdoc/updateState')

export const removePrintdoc = createAsyncThunk(
  'printdoc/remove',
  async (payload: { id: number; doc: number }): Promise<number> => {
    const { id, doc } = payload
    fileService.removeFile(doc)
    const removeId = await printdocService.removePrintdoc(id)
    return removeId
  }
)

// Slice
const slice = createSlice({
  name: 'printdoc',
  initialState: entityAdapter.getInitialState(),
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getPrintdocs.fulfilled, entityAdapter.setAll)
      .addCase(updatePrintdocState, (state, { payload }) => {
        const entity = state.entities[payload.id]
        entity.status = payload.status
      })
      .addCase(updatePrintdocRead.fulfilled, (state, { payload }) => {
        payload.forEach((item) => {
          const entity = state.entities[item.id]
          entity.isRead = item.isRead
        })
      })
      .addCase(createPrintdoc.fulfilled, entityAdapter.upsertOne)
      .addCase(removePrintdoc.fulfilled, entityAdapter.removeOne)
  }
})

export default slice.reducer
