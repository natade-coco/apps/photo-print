export const REPO_URL = 'https://gitlab.com/natade-coco/apps/bootstrap'
export const ETHERSCAN_URL = 'https://etherscan.io'
export const MPM_GUIDELINE_URL =
  'https://www.paymentsjapan.or.jp/wordpress/wp-content/uploads/2020/04/MPM_Guideline_2.0.pdf'
export const JPQR_URL = 'https://jpqr-start.jp/'
