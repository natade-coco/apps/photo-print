export type FrameDirection = 'horizontally' | 'vertically'
export interface Photoframe {
  id: number
  frame: any
  direction: FrameDirection
}
