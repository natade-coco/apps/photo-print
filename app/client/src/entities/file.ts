export interface File {
  data: string
  name: string
}
