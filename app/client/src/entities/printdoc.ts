export interface Printdoc {
  id: number
  dateCreated: string
  dateUpdated: string
  did: string
  doc: any
  status: JobState
  reference: number
  state: 'published' | 'draft'
  frameId: number
  isRead: boolean
  isVertically: boolean
}

export type PrintdocParam = Omit<Partial<Printdoc>, 'id' | 'dateCreated' | 'dateUpdated'>

export type JobState = 'pending' | 'uploaded' | 'processing' | 'completed' | 'received' | 'aborted'
