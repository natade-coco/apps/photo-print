export interface Printcount {
  id: number
  dateUpdated: string
  did: string
  value: number
  total: number
}
