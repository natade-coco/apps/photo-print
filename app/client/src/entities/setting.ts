export interface Setting {
  id: string
  dateCreated: string
  printLimit: number
  isPaid: boolean
  hasNumber: boolean
  printAmount?: number
  stripeAccount?: string
}
