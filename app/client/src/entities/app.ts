interface User {
  id: string | null
  printCount: number
  printId: number
  printTotal: number
}

interface App {
  id: string | null
}

export interface Session {
  token: string | null
  user: User | null
  app: App | null
  env: 'prd' | 'stg'
}

export type CanvasData = Partial<
  Cropper.CanvasData & { rotate: number; withFrame: boolean; scaleY: number; scaleX: number }
>

export type Id = string | number
