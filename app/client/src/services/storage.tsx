export interface Document {
  id: string
  date_updated: string
  status: string
  reference: number
  doc: string
}

export class DocumentStorage {
  KEY: string = 'document'
  storage = typeof window !== 'undefined' ? window.localStorage : undefined

  setDocument = (document: Document) => {
    const value = JSON.stringify(document)
    this.storage?.setItem(this.KEY, value)
  }

  getDocument = (): Document | null => {
    const value = this.storage?.getItem(this.KEY)
    if (!value) return null
    return JSON.parse(value)
  }

  clearDocument = () => {
    this.storage?.removeItem(this.KEY)
  }
}
