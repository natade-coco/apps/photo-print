import Pocket from '@natade-coco/pocket-sdk'

export default class PocketService {
  requestSignJWT = () => {
    if (process.env.NODE_ENV === 'development') {
      return Promise.resolve(process.env.GATSBY_APP_TOKEN)
    }
    return Pocket.requestSignJWT()
  }
}
