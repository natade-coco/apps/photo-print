import { Application, FeathersService, Service } from '@feathersjs/feathers/lib'
import { serverApp } from './index'
import { Printcount } from '../../entities/printcount'

export default class PrintcountService {
  service: FeathersService<Application, Service<Printcount>>
  constructor() {
    this.service = serverApp?.service('printcounts')
  }

  getPrintcount = async (did: string) => {
    const result = await this.service.find({
      query: {
        filter: {
          did: {
            _eq: did
          }
        }
      }
    })
    if (result instanceof Array) {
      return result[0]
    }
    return result
  }

  updatePrintcount = async (id: number, value: number, total?: number) => {
    console.log({ id, value, total })
    const result = await this.service.update(id, { value, total })
    if (result instanceof Array) {
      return result[0]
    }
    return result
  }
}
