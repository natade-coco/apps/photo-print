import { Application, FeathersService, Id, Service } from '@feathersjs/feathers/lib'
import { serverApp } from './index'
import { Setting } from '../../entities/setting'

export default class SettingService {
  service: FeathersService<Application, Service<Setting>>
  constructor() {
    this.service = serverApp?.service('printsettings')
  }
  getSettings = async (): Promise<Setting> => {
    const result = await this.service.find()
    if (result instanceof Array) {
      return result[0]
    }
    return result
  }
}
