import { Application, FeathersService, Service } from '@feathersjs/feathers/lib'
import { File } from '../../entities/file'
import { serverApp } from './index'

export default class FileService {
  service: FeathersService<Application, Service<File>>
  constructor() {
    this.service = serverApp?.service('files')
  }

  createFile = async (data: FormData | any) => {
    const result = this.service.create({ data })
    return result
  }

  removeFile = async (id: number) => {
    const result = this.service.remove(id)
    if (result instanceof Array) {
      return result[0]
    }
    return result
  }
}
