import { Session } from '../../entities/app'
import { Printdoc } from '../../entities/printdoc'
import { serverApp } from './index'

export default class AppService {
  getSession = (jwt?: string): Promise<Session> => {
    return serverApp.service('authentication').create({ strategy: 'natadecoco', token: jwt })
  }
  subscribeUpdatePrintState = (cb: (doc: Printdoc) => void) => {
    serverApp.service('hooks').on('created', cb)
  }
}
