import { Application, FeathersService, Service } from '@feathersjs/feathers/lib'
import { Photoframe } from '../../entities/photoframe'
import { serverApp } from './index'

export default class PhotoframeService {
  service: FeathersService<Application, Service<Photoframe>>
  constructor() {
    this.service = serverApp?.service('photoframes')
  }

  getPhotoframes = async () => {
    const result = await this.service.find({
      query: {
        fields: ['*', 'frame.data.*'],
        filter: {
          status: {
            _eq: 'published'
          }
        },
        sort: 'sort'
      }
    })
    if (result instanceof Array) {
      return result
    }
    return [result]
  }
}
