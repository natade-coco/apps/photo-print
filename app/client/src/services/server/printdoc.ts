import { Application, FeathersService, Service } from '@feathersjs/feathers/lib'
import { serverApp } from './index'
import { JobState, Printdoc } from '../../entities/printdoc'

export default class PrintdocService {
  service: FeathersService<Application, Service<Printdoc>>
  constructor() {
    this.service = serverApp?.service('printdocs')
  }

  getPrintdocs = async (did: string) => {
    const result = await this.service.find({
      query: {
        filter: {
          did: {
            _eq: did
          },
          state: {
            _eq: 'published'
          }
        },
        sort: ['-date_created']
      }
    })
    if (result instanceof Array) {
      return result
    }
    return [result]
  }

  createPrintdoc = async (
    doc: any,
    did: string,
    reference: number,
    frameId: number,
    isVertically: boolean,
    status: JobState = 'uploaded'
  ) => {
    const result = await this.service.create({
      doc,
      did,
      reference,
      status,
      frameId,
      isVertically
    })
    return result
  }

  updatePrintdoc = async (id: number, status: JobState) => {
    const result = await this.service.update(id, { status })
    if (result instanceof Array) {
      return result[0]
    }
    return result
  }

  updateReadPrintdocs = async (ids: number[]) => {
    const result = await this.service.patch(ids as any, { isRead: true })
    return result
  }

  removePrintdoc = async (id: number) => {
    await this.service.remove(id)
    return id
  }
}
