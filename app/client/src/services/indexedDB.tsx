export interface Document {
  id: string
  date_updated: string
  status: string
  reference: number
  doc: string
}

const PHOTO_PRINT_DATABASE = 'PhotoPrintDatabase'

export class ImageIndexedDB {
  KEY: string = 'images'
  storage = typeof window !== 'undefined' ? window.indexedDB : undefined

  constructor() {
    if (!this.storage) return
    const request = this.storage.open(PHOTO_PRINT_DATABASE, 3)

    request.onupgradeneeded = (event) => {
      const dbReq: IDBOpenDBRequest = event.target as IDBOpenDBRequest
      const db = dbReq.result as IDBDatabase
      if (db.objectStoreNames.contains(this.KEY)) {
        db.deleteObjectStore(this.KEY)
      }
      const store = db.createObjectStore(this.KEY, { keyPath: 'name' })
      store.add({ name: 'selected_image', src: '' })
    }
  }

  setImage = (src: string, cb?: () => void) => {
    const request = this.storage.open(PHOTO_PRINT_DATABASE, 3)
    request.onsuccess = (event) => {
      const dbReq: IDBOpenDBRequest = event.target as IDBOpenDBRequest
      const db = dbReq.result as IDBDatabase
      const imageObjectStore = db.transaction([this.KEY], 'readwrite').objectStore(this.KEY)
      const request = imageObjectStore.put({ name: 'selected_image', src })
      request.onsuccess = () => {
        cb && cb()
      }
    }
  }

  getImage = (cb: (src: string) => void) => {
    const request = this.storage.open(PHOTO_PRINT_DATABASE, 3)
    request.onsuccess = (event) => {
      const dbReq: IDBOpenDBRequest = event.target as IDBOpenDBRequest
      const db = dbReq.result as IDBDatabase
      const imageObjectStore = db.transaction([this.KEY], 'readonly').objectStore(this.KEY)
      const request = imageObjectStore.get('selected_image')
      request.onsuccess = () => {
        cb(request.result.src)
      }
    }
  }

  clearImage = (cb: () => void) => {
    const request = this.storage.open(PHOTO_PRINT_DATABASE, 3)
    request.onsuccess = (event) => {
      const dbReq: IDBOpenDBRequest = event.target as IDBOpenDBRequest
      const db = dbReq.result as IDBDatabase
      const imageObjectStore = db.transaction([this.KEY], 'readwrite').objectStore(this.KEY)
      const request = imageObjectStore.put({ name: 'selected_image', src: '' })
      request.onsuccess = () => {
        cb()
      }
    }
  }
}
