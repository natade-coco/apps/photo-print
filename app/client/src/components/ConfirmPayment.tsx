import { Payout } from '@natade-coco/react-pay-js'
import * as React from 'react'

type Props = {
  onClose: () => void
  onCompleted: (txId: string, status: string, chargeId: string, receiptUrl: string) => boolean
  onError: (reason: any) => void
  showPayment: boolean
  account: string
  amount: number
}

const ConfirmPayment = (props: Props) => {
  return (
    <div className={`modal ${props.showPayment ? 'is-active' : ''}`}>
      <div className="modal-background"></div>
      <div className="modal-content">
        <div className="box mx-2">
          <div className="has-text-right mb-3">
            <button className="delete" aria-label="close" onClick={props.onClose}></button>
          </div>
          <div className="box" style={{ background: '#e5f6ff' }}>
            <div className="is-size-5 has-text-weight-bold has-text-centered" style={{ whiteSpace: 'nowrap' }}>
              お支払い内容
            </div>
            <div className="is-relative" style={{ height: '3rem' }}>
              <span style={{ position: 'absolute', bottom: 0 }}>プリント料金:</span>
              <span style={{ position: 'absolute', right: 0, bottom: 0, lineHeight: '2rem' }}>
                <span className="is-size-3">{props.amount}</span>円
              </span>
            </div>
            <div className="has-text-centered mt-5">
              <Payout
                account={props.account}
                style={{ margin: 'auto' }}
                amount={props.amount}
                opts={{
                  test: true,
                  identityHubToken: process.env.GATSBY_APP_IDENTITYHUB_JWT || undefined,
                  paymentHubToken: process.env.GATSBY_APP_PAYMENTHUB_JWT || undefined
                }}
                onCompleted={props.onCompleted}
                onError={props.onError}
              />
            </div>
          </div>
          <div className="is-size-6">内容をご確認後、「ココペイでお支払い」からお支払い手続きへお進みください。</div>
        </div>
      </div>
    </div>
  )
}

export default ConfirmPayment
