import React from 'react'

type PrintBtnProps = {
  onClickPrint: () => void
  isLoading: boolean
  isDisabled: boolean
  amount: number
  count: number
  test?: boolean
}

const PrintBtn = (props: PrintBtnProps) => {
  return (
    <>
      <div
        className="has-text-centered is-flex is-flex-direction-column is-justify-content-space-evenly"
        style={{ minHeight: '100%' }}
      >
        <div>
          <button
            className={`is-relative button coco-btn is-large ${
              props.isLoading && 'is-loading'
            } has-text-weight-bold is-rounded has-text-white is-disabled`}
            onClick={props.onClickPrint}
            style={{
              padding: '1rem',
              opacity: `${props.isDisabled ? '0.5' : '1'}`,
              pointerEvents: `${props.isDisabled ? 'none' : 'unset'}`
            }}
          >
            {!props.isLoading && 'プリントする'}
            <div style={{ position: 'absolute', right: '1rem' }}>
              <span className="icon has-text-white">
                <i className="fas fa-chevron-right"></i>
              </span>
            </div>
          </button>
        </div>
      </div>
    </>
  )
}

export default PrintBtn
