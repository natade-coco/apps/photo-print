import * as React from 'react'
import error from '../images/img_error.png'

type SizeLimitProps = {
  showNotice: boolean
  onClose: () => void
  title: string
  message: string
}

const ErrorModal = ({ showNotice, message, onClose, title }: SizeLimitProps) => {
  return (
    <div className={`modal ${showNotice ? 'is-active' : ''}`}>
      <div className="modal-background"></div>
      <div className="modal-content" style={{ maxHeight: 'none' }}>
        <div className="box ml-2 mr-2">
          <div className="is-size-5 has-text-centered has-text-weight-bold">{title}</div>
          <div className="has-text-centered">
            <img src={error} width={160} />
          </div>
          <span className="is-size-6 mt-2 mx-1">{message}</span>
          <div className="has-text-centered mt-5">
            <button className="button is-dark is-medium" onClick={onClose}>
              とじる
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ErrorModal
