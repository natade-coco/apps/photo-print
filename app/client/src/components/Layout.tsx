import * as React from 'react'
import { Helmet } from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

interface DefaultLayoutProps extends React.HTMLProps<HTMLDivElement> {
  children: any
}

class DefaultLayout extends React.PureComponent<DefaultLayoutProps, {}> {
  public render() {
    return (
      <StaticQuery
        query={graphql`
          query LayoutQuery {
            site {
              siteMetadata {
                title
              }
            }
          }
        `}
        render={(data) => (
          <>
            <Helmet
              htmlAttributes={{
                class: ''
              }}
              titleTemplate={`%s | ${data.site.siteMetadata.title}`}
              defaultTitle={data.site.siteMetadata.title}
              meta={[
                { name: 'description', content: 'photo-print' },
                { name: 'keywords', content: 'natadeCOCO' }
              ]}
              script={[{ src: 'https://use.fontawesome.com/releases/v5.3.1/js/all.js', defer: true }]}
              link={[
                {
                  href: 'https://fonts.googleapis.com/css2?family=M+PLUS+1p:wght@400;500;700&display=swap',
                  rel: 'stylesheet'
                }
              ]}
            />
            {this.props.children}
          </>
        )}
      />
    )
  }
}

export default DefaultLayout
