import React, { useEffect, useRef, useState } from 'react'
import Slider, { Settings } from 'react-slick'
import { Photoframe } from '../entities/photoframe'
export const HEIGHT = 1052
export const WIDTH = 1407

type FrameProps = {
  photoframes: Photoframe[]
  isLoading: boolean
  onChangeFrameNum: (num: number) => void
  selectedImage?: string
  verticallySelectedImage?: string
  selectedFrameNum: number
  onClickEditImageBtn: () => void
  onFinishDraw: () => void
  focusRef: React.MutableRefObject<HTMLElement>
}

const Frame = (props: FrameProps) => {
  return (
    <>
      {props.photoframes.length > 0 ? (
        <div>
          <div className="has-text-centered">
            <canvas
              id="canvas-preview"
              style={{ maxWidth: '90vw', width: 'auto', maxHeight: '66vw', height: 'auto', margin: '1rem 0' }}
            ></canvas>
          </div>
          <div style={{ position: 'relative', maxWidth: '500px', width: '100%', margin: '0 auto' }}>
            <FrameSelector {...props} onFinishDraw={props.onFinishDraw} />
            <FocusFrame reference={props.focusRef} />
          </div>
        </div>
      ) : (
        !props.isLoading && (
          <div
            className="is-relative has-background-grey-dark has-text-white has-text-centered has-text-weight-bold"
            style={{ height: '50vh' }}
          >
            <span style={{ position: 'absolute', inset: '0px', margin: 'auto', height: '1rem', width: '100%' }}>
              フレームが登録されていません
            </span>
          </div>
        )
      )}
    </>
  )
}

export default Frame

const FocusFrame = (props: { reference: React.MutableRefObject<any> }) => {
  return (
    <div
      ref={props.reference}
      style={{
        border: 'solid 3px white',
        position: 'absolute',
        top: -3,
        left: 0,
        right: 0,
        margin: '0 auto',
        width: 0,
        height: 0,
        pointerEvents: 'none',
        display: 'none'
      }}
    ></div>
  )
}

type FrameSelectorProps = {
  photoframes: Photoframe[]
  onChangeFrameNum: (num: number) => void
  selectedImage?: string
  verticallySelectedImage?: string
  selectedFrameNum: number
  onFinishDraw: () => void
}

const FrameSelector = (props: FrameSelectorProps) => {
  useEffect(() => {
    let cnt = 0
    props.photoframes.forEach((item, index) => {
      const src = item.frame
      const canvas = document.getElementById(`canvas-${index}`) as HTMLCanvasElement
      if (canvas) {
        const selectedImage = new Image()
        const frameImage = new Image()
        const ctx = canvas.getContext('2d')
        frameImage.onload = () => {
          ctx.drawImage(frameImage, 0, 0, canvas.width, canvas.height)
          cnt++
          if (cnt === props.photoframes.length) {
            props.onFinishDraw()
          }
        }
        selectedImage.onload = () => {
          ctx.drawImage(selectedImage, 0, 0, canvas.width, canvas.height)
          toDataURL(src, (data: any) => {
            frameImage.src = data
          })
        }
        selectedImage.src = item.direction === 'vertically' ? props.verticallySelectedImage : props.selectedImage
      }
    })
  }, [props.photoframes, props.selectedImage, props.verticallySelectedImage])

  const toDataURL = (url: string, cb: (result: any) => void) => {
    const xhr = new XMLHttpRequest()
    xhr.onload = () => {
      const reader = new FileReader()
      reader.onloadend = () => {
        cb(reader.result)
      }
      reader.readAsDataURL(xhr.response)
    }
    xhr.open('GET', url)
    xhr.responseType = 'blob'
    xhr.send()
  }

  const settings: Settings = {
    infinite: false,
    slidesToShow: 3,
    arrows: false,
    afterChange: (current) => {
      props.onChangeFrameNum(current)
      const target = document.getElementById(`canvas-${current}`) as HTMLCanvasElement
      const preview = document.getElementById('canvas-preview') as HTMLCanvasElement
      preview.width = props.photoframes[current]?.direction === 'vertically' ? HEIGHT : WIDTH
      preview.height = props.photoframes[current]?.direction === 'vertically' ? WIDTH : HEIGHT
      const ctx = preview.getContext('2d')
      ctx.drawImage(target, 0, 0)
    },
    initialSlide: props.selectedFrameNum,
    centerMode: true,
    swipeToSlide: true,
    focusOnSelect: true,
    dots: false
  }

  return (
    <>
      <Slider {...settings}>
        {props.photoframes?.map((item, index) => (
          <div key={index + '-slider'} className="pl-1 pr-1 has-text-centered">
            <canvas
              id={`canvas-${index}`}
              width={item.direction === 'vertically' ? HEIGHT : WIDTH}
              height={item.direction === 'vertically' ? WIDTH : HEIGHT}
              style={{ width: '100%', height: 'auto' }}
            ></canvas>
          </div>
        ))}
        <div></div>
        <div></div>
      </Slider>
    </>
  )
}
