import React, { useState } from 'react'

export type EditMode = 'frame' | 'adjust'
export type EditMenuProps = {
  mode: EditMode
  isModified: boolean
  onClickMenu: (mode: EditMode) => void
}

const EditMenu = ({ mode, onClickMenu, isModified }: EditMenuProps) => {
  return (
    <div className="" style={{ width: '100%' }}>
      <div className="tabs has-text-white">
        <ul
          className="edit-menu-tabs has-text-weight-medium"
          style={{ justifyContent: 'space-around', fontSize: '18px' }}
        >
          <li
            className={`${mode === 'frame' && 'is-active'}`}
            onClick={() => onClickMenu('frame')}
            style={isModified ? { pointerEvents: 'none' } : {}}
          >
            <a style={isModified ? { color: '#84808022' } : {}}>フレーム選択</a>
          </li>
          <li className={`${mode === 'adjust' && 'is-active'}`} onClick={() => onClickMenu('adjust')}>
            <a>写真を調整</a>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default EditMenu
