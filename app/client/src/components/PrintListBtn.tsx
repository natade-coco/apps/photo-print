import { navigate } from 'gatsby-link'
import React from 'react'

export const PrintListBtn = (props: { count: number }) => {
  return (
    <div className="is-relative print-list-btn" onTouchEnd={() => navigate('/history')}>
      {!!props.count && (
        <div className="badge">
          <span>{props.count}</span>
        </div>
      )}
      <span className="fa-stack">
        <i className="fas fa-circle fa-stack-2x" style={{ color: '#09b4c6', fontSize: '2.3rem' }}></i>
        <i className="fas fa-circle fa-stack-2x has-text-white" style={{ fontSize: '2rem' }}></i>
        <i className="fas fa-list fa-stack-1x" style={{ color: '#09b4c6', fontSize: '2rem' }}></i>
      </span>
    </div>
  )
}
