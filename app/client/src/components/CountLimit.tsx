import * as React from 'react'
import error from '../images/img_error.png'

type CountLimitProps = {
  showNotice: boolean
  printLimit: number
  onClose: () => void
}

const CountLimit = (props: CountLimitProps) => {
  return (
    <div className={`modal ${props.showNotice ? 'is-active' : ''}`}>
      <div className="modal-background"></div>
      <div className="modal-content">
        <div className="box ml-2 mr-2">
          <div className="is-size-5 has-text-weight-bold has-text-centered" style={{ whiteSpace: 'nowrap' }}>
            本日の印刷回数の上限を超えました
          </div>
          <div className="has-text-centered">
            <img src={error} width={160} />
          </div>
          <div className="is-size-6 has-text-weight-bold">{`１日の印刷回数をお一人様、${props.printLimit}回までとさせていただいております。またのご利用をお待ちしています。`}</div>
          <div className="has-text-centered mt-5">
            <button className="button is-dark is-medium" onClick={props.onClose}>
              閉じる
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CountLimit
