import React from 'react'

type PhotoEditConfirmProps = {
  onClickConfirm: () => void
  onClickCancel: () => void
  isLoading: boolean
}

const PhotoEditConfirm = (props: PhotoEditConfirmProps) => {
  const { onClickCancel, onClickConfirm } = props
  return (
    <div className="is-flex is-justify-content-space-evenly">
      <div className="has-text-centered">
        <button
          className={`button is-large has-text-white has-text-weight-medium`}
          style={{ backgroundColor: '#848080', border: '0.25rem solid white', height: '3.3rem' }}
          onClick={onClickCancel}
          disabled={props.isLoading}
        >
          キャンセル
        </button>
      </div>
      <div className="has-text-centered">
        <button
          className={`button is-large has-text-white has-text-weight-medium ${props.isLoading ? 'is-loading' : ''}`}
          style={{ height: '3.3rem', border: '0.25rem solid white', background: '#09b4c6' }}
          onClick={onClickConfirm}
        >
          決定
        </button>
      </div>
    </div>
  )
}

export default PhotoEditConfirm
