import * as React from 'react'

type SelectorProps = {
  remainCount: number
  isLoading: boolean
  onClickSelector: () => void
  onSelectedImage: (file: React.ChangeEvent) => void
}

export const Selector = (props: SelectorProps) => {
  return (
    <div className="has-text-centered">
      <button
        className={`is-relative coco-btn button is-large has-text-white ${
          props.isLoading && 'is-loading'
        } has-text-weight-bold is-rounded`}
        onClick={() => {
          props.onClickSelector()
        }}
      >
        {!props.isLoading && '写真をえらぶ'}
        <div style={{ position: 'absolute', right: '1rem' }}>
          {/* <img style={{ width: "30px", height: "auto" }} src={arrowImg} /> */}
          <span className="icon has-text-white">
            <i className="fas fa-chevron-right"></i>
          </span>
        </div>
      </button>

      <input
        type="file"
        id="selectedImage"
        accept="image/*"
        onChange={props.onSelectedImage}
        style={{ display: 'none' }}
      />
    </div>
  )
}
