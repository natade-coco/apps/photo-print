import * as React from 'react'
import { JobState } from '../entities/printdoc'

export const PrintState = (props: { state: JobState }) => {
  switch (props.state) {
    case 'uploaded':
      return (
        <span className={`tag is-light is-size-7 is-warning ml-1`} style={{ verticalAlign: 'middle' }}>
          準備中
        </span>
      )
    case 'processing':
      return (
        <span className={`tag is-light is-size-7 is-info ml-1`} style={{ verticalAlign: 'middle' }}>
          印刷中
        </span>
      )
    case 'completed':
      return (
        <span className={`tag is-light is-size-7 is-danger ml-1`} style={{ verticalAlign: 'middle' }}>
          受取可
        </span>
      )
    case 'received':
      return (
        <span className={`tag is-light is-size-7 ml-1`} style={{ verticalAlign: 'middle' }}>
          受取済み
        </span>
      )
    case 'aborted':
      return (
        <span className={`tag is-light is-size-7 is-dark ml-1`} style={{ verticalAlign: 'middle' }}>
          失敗
        </span>
      )
    default:
      return (
        <span className={`tag is-light is-size-7 is-dark ml-1`} style={{ verticalAlign: 'middle' }}>
          不明
        </span>
      )
  }
}
