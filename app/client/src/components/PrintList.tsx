import * as React from 'react'
import { Printdoc } from '../entities/printdoc'
import { PrintState } from './PrintState'
import { parseDate } from '../util'

type PrintListProps = {
  histories: Printdoc[]
  isLoading: boolean
}

const PrintList = (props: PrintListProps) => (
  <div className="mr-2 ml-2">
    {!props.isLoading && (
      <>
        {props.histories.length > 0 ? (
          props.histories.map((printDoc, index) => (
            <article
              key={index}
              className="print-list-item media box"
              style={{ background: '#6b9dbd', border: '3px solid white' }}
            >
              <div className="media-content has-text-white">
                <div className="content">
                  <p>
                    受付番号: <strong className="is-size-3 has-text-white">{printDoc.reference}</strong>
                    <PrintState state={printDoc.status} />
                    <br />
                    <span className="is-size-7 has-text-white">
                      更新日時: {parseDate(printDoc.dateUpdated || printDoc.dateCreated)}
                    </span>
                  </p>
                </div>
              </div>
              <figure
                className="media-right is-flex is-flex-direction-row is-justify-content-center"
                style={{ margin: 'auto', width: '90px' }}
              >
                <p className="image">
                  <img src={printDoc.doc} style={{ height: '72px', width: 'auto' }} />
                </p>
              </figure>
            </article>
          ))
        ) : (
          <article className="message is-size-6" style={{ background: '#566f7e', borderTop: 'none' }}>
            <div className="message-body" style={{ borderLeft: 'none' }}>
              <span className="has-text-white has-text-weight-bold">履歴がありません</span>
            </div>
          </article>
        )}
      </>
    )}
  </div>
)

export default PrintList
