import * as React from 'react'
import error from '../images/img_error.png'

type SizeLimitProps = {
  showNotice: boolean
  onClose: () => void
}

const SizeLimit = (props: SizeLimitProps) => {
  return (
    <div className={`modal ${props.showNotice ? 'is-active' : ''}`}>
      <div className="modal-background"></div>
      <div className="modal-content" style={{ maxHeight: 'none' }}>
        <div className="box ml-2 mr-2">
          <div className="is-size-5 has-text-centered has-text-weight-bold">写真サイズが上限を超えています</div>
          <div className="has-text-centered">
            <img src={error} width={160} />
          </div>
          <span className="is-size-6 mt-2 mx-1">
            印刷可能な写真サイズの上限は <span className="has-text-weight-bold">30MB</span>{' '}
            です。他の写真を選択してください。
          </span>
          <div className="has-text-centered mt-5">
            <button className="button is-dark is-medium" onClick={props.onClose}>
              とじる
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SizeLimit
