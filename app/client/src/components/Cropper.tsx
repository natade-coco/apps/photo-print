import React, { useEffect, useRef, useState } from 'react'
import Cropper, { ReactCropperElement } from 'react-cropper'
import 'cropperjs/dist/cropper.css'
import rotationIcon from '../images/rotate.svg'
import inversionIcon from '../images/inversion.svg'
import { HEIGHT, WIDTH } from './Frame'
import { CanvasData } from '../entities/app'

type CropperComponentProps = {
  src: string
  cropperOption?: CanvasData
  cropperRef: React.MutableRefObject<ReactCropperElement>
  selectedFrame: string
  onModify: () => void
  isVertically: boolean
  isEditable: boolean
}

const CropperComponent = (props: CropperComponentProps) => {
  const cropperRef = props.cropperRef
  const [withFrame, setWithFrame] = useState(true)
  useEffect(() => {
    const frameImage = document.getElementById('frame-on-cropper') as HTMLImageElement
    if (frameImage) {
      frameImage.src = props.selectedFrame
    }
  }, [props.selectedFrame])

  const zooming = (event: Cropper.ZoomEvent) => {
    const cropper = cropperRef.current!.cropper
    const isZoomIn = event.detail.ratio > event.detail.oldRatio
    const imageData = cropper.getCanvasData()
    const boxData = cropper.getCropBoxData()
    const zoomLevel = imageData?.width / boxData.width
    if ((isZoomIn && zoomLevel > 3) || (!isZoomIn && zoomLevel < 0.5)) {
      event.preventDefault()
      return
    } else {
      if (['pointermove', 'wheel'].includes(event.detail.originalEvent?.type)) {
        props.onModify()
      }
    }
  }

  const rotate = (angle: number) => {
    const cropper = cropperRef.current?.cropper
    cropper.rotate(angle)
    props.onModify()
  }

  const inversion = () => {
    const cropper = cropperRef.current?.cropper
    const rotateVol = cropper.getData().rotate
    if (Math.abs(rotateVol) % 180 === 0) {
      const volX = cropper.getData().scaleX * -1
      cropper.scaleX(volX)
    } else {
      const volY = cropper.getData().scaleY * -1
      cropper.scaleY(volY)
    }
    props.onModify()
  }

  const ready = () => {
    const cropper = cropperRef.current!.cropper
    const containerData = cropper.getContainerData()
    cropper
      .setCropBoxData({
        height: containerData.height * 0.85
      })
      .setCropBoxData({
        left: (containerData.width - cropper.getCropBoxData().width) / 2
      })
      .setCropBoxData({
        top: (containerData.height - cropper.getCropBoxData().height) / 2
      })

    const boxData = cropper.getCropBoxData()
    const canvasData = cropper.getCanvasData()
    if (props.isVertically) {
      if (canvasData.height / canvasData.width > HEIGHT / WIDTH) {
        cropper
          .setCanvasData({
            width: boxData.width,
            left: boxData.left
          })
          .setCanvasData({ top: (containerData.height - cropper.getCanvasData().height) / 2 })
      } else {
        cropper
          .setCanvasData({
            height: boxData.height,
            top: boxData.top
          })
          .setCanvasData({ left: (containerData.width - cropper.getCanvasData().width) / 2 })
      }
    } else {
      if (canvasData.height / canvasData.width > HEIGHT / WIDTH) {
        cropper
          .setCanvasData({
            width: boxData.height * (HEIGHT / WIDTH)
          })
          .setCanvasData({
            top: (containerData.height - cropper.getCanvasData().height) / 2,
            left: (containerData.width - cropper.getCanvasData().width) / 2
          })
      } else {
        cropper
          .setCanvasData({
            height: boxData.height,
            top: boxData.top
          })
          .setCanvasData({ left: (containerData.width - cropper.getCanvasData().width) / 2 })
      }
    }

    const cropperContainer = document.querySelector('#image-edit-cropper .cropper-container') as HTMLElement
    const cropperBox = document.querySelector('#image-edit-cropper .cropper-crop-box') as HTMLElement
    const frameImage = document.createElement('img') as HTMLImageElement
    frameImage.id = 'frame-on-cropper'
    const transForm = cropperBox.style.transform
    const width = cropperBox.style.width
    setWithFrame(props.cropperOption?.withFrame ?? withFrame)
    frameImage.src = props.selectedFrame
    frameImage.style.transform = transForm
    frameImage.style.width = width
    frameImage.style.height = 'auto'
    frameImage.style.position = 'absolute'
    frameImage.style.zIndex = '1'
    frameImage.style.pointerEvents = 'none'
    frameImage.style.opacity = Number(props.cropperOption?.withFrame ?? withFrame).toString()
    cropperContainer.appendChild(frameImage)
  }

  const onFrame = () => {
    const frameImage = document.getElementById('frame-on-cropper') as HTMLImageElement
    frameImage.style.opacity = Number(!withFrame).toString()
    setWithFrame(!withFrame)
  }

  const cropmove = (event: Cropper.CropMoveEvent) => {
    const type = event.detail.originalEvent?.type
    if (type === 'pointermove' || type === 'touchmove') {
      props.onModify()
    }
  }
  return (
    <div style={{ pointerEvents: props.isEditable ? 'unset' : 'none' }}>
      <div className="is-flex is-justify-content-center" id="image-edit-cropper">
        {/* <img id="frame-on-cropper" src={props.selectedFrame} style={{ display: "none" }} /> */}
        <Cropper
          ready={ready}
          ref={cropperRef}
          src={props.src}
          guides={true}
          dragMode="move"
          // minContainerWidth={window.outerWidth * 0.9}
          // minContainerHeight={window.outerWidth * 0.7}
          center={false}
          cropmove={cropmove}
          background={false}
          // style={{ maxHeight: "50vh", height: "auto", width: "90%", margin: "1rem" }}
          style={{ width: '90vw', maxHeight: '50vh', margin: '1rem 0', height: '69vw' }}
          cropBoxResizable={false}
          viewMode={0}
          aspectRatio={props.isVertically ? 1052 / 1407 : 1407 / 1052}
          cropBoxMovable={false}
          zoom={zooming}
          restore={false}
          autoCropArea={1}
          responsive={true}
          checkOrientation={false}
        />
      </div>
      <div style={{ display: 'flex', justifyContent: 'space-around', touchAction: 'manipulation' }}>
        <div
          style={{
            display: 'flex',
            height: '70px',
            width: '70px',
            border: `3px solid ${withFrame ? '#09b4c6' : 'white'}`,
            borderRadius: '50%',
            justifyContent: 'center',
            flexDirection: 'column'
          }}
          onClick={onFrame}
        >
          <div
            style={{
              textAlign: 'center',
              fontSize: '12px',
              color: `${withFrame ? '#09b4c6' : '#f6f6f6'}`,
              textShadow: `${
                withFrame
                  ? '0px 0px 1px white, 0px 0px 1px white, 0px 0px 1px white, 0px 0px 1px white, 0px 0px 1px white'
                  : 'none'
              }`,
              fontWeight: 900
            }}
          >
            フレーム
            <br />
            {`${withFrame ? '表示' : '非表示'}`}
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            height: '70px',
            width: '70px',
            border: '2px solid white',
            borderRadius: '50%',
            justifyContent: 'center',
            flexDirection: 'column'
          }}
          onClick={() => rotate(-90)}
        >
          <img className="is-rounded" src={rotationIcon} style={{ height: '29px' }} />
          <div style={{ textAlign: 'center', fontSize: '14px', color: 'white' }}>-90°</div>
        </div>
        <div
          style={{
            display: 'flex',
            height: '70px',
            width: '70px',
            border: '2px solid white',
            borderRadius: '50%',
            justifyContent: 'center',
            flexDirection: 'column'
          }}
          onClick={() => rotate(90)}
        >
          <img className="is-rounded" src={rotationIcon} style={{ height: '29px', transform: 'scale(-1, 1)' }} />
          <div style={{ textAlign: 'center', fontSize: '14px', color: 'white' }}>+90°</div>
        </div>
        <div
          style={{
            display: 'flex',
            height: '70px',
            width: '70px',
            border: '2px solid white',
            borderRadius: '50%',
            justifyContent: 'center',
            flexDirection: 'column'
          }}
          onClick={() => inversion()}
        >
          <img className="is-rounded" src={inversionIcon} style={{ height: '31px', transform: 'translateY(6px)' }} />
          <div style={{ textAlign: 'center', fontSize: '14px', color: 'white' }}>反転</div>
        </div>
      </div>
    </div>
  )
}

export default CropperComponent
