import { navigate } from 'gatsby-link'
import * as React from 'react'

const Footer = (props: {}) => {
  const paths = ['/terms', '/privacy']
  const futterProps = [
    { iconName: 'fas fa-file-alt', menuName: '・利用規約', path: '/terms' },
    { iconName: 'fas fa-lock', menuName: '・プライバシーポリシー', path: '/privacy' }
  ]
  const [path, setPath] = React.useState('')
  React.useEffect(() => {
    setPath(location.pathname)
  })

  return (
    !(paths.indexOf(path) > -1) && (
      <footer className="footer-area">
        {futterProps.map((item, index) => (
          <div key={`footer-columns-${index}`} onClick={() => navigate(item.path)}>
            {item.menuName}
          </div>
        ))}
      </footer>
    )
  )
}

export default Footer
