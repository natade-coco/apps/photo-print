import * as React from 'react'
import error from '../images/img_error.png'

type CountLimitProps = {
  showNotice: boolean
  onClose: () => void
  error: string
}

const Timeout = (props: CountLimitProps) => {
  return (
    <div className={`modal ${props.showNotice ? 'is-active' : ''}`}>
      <div className="modal-background"></div>
      <div className="modal-content">
        <div className="box ml-2 mr-2">
          <div className="is-size-5 has-text-weight-bold has-text-centered" style={{ whiteSpace: 'nowrap' }}>
            サーバーとの接続に失敗しました。
          </div>
          <div className="has-text-centered">
            <img src={error} width={160} />
          </div>
          <div className="is-size-6 has-text-weight-bold">{`ページの再読み込みを行ってください。`}</div>
          <div className="is-size-7 is-danger">{props.error}</div>
          <div className="has-text-centered mt-5">
            <button className="button is-medium" onClick={props.onClose}>
              再読み込み
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Timeout
