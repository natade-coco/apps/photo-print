import { Link } from 'gatsby'
import * as React from 'react'
import success from '../images/img_success.png'
import { DocumentStorage } from '../services/storage'

type PrintResultProps = {
  isLoading?: boolean
  showResult: boolean
  reference: number
  onClose: () => void
}
const storage = new DocumentStorage()

const PrintResult = (props: PrintResultProps) => {
  return (
    <div className={`modal ${props.showResult && 'is-active'}`}>
      <div className="modal-background"></div>
      <div className="modal-content " style={{ maxHeight: 'none' }}>
        <div className="box ml-2 mr-2 has-text-white" style={{ backgroundColor: '#a0cdd6' }}>
          <div className="is-size-4 has-text-weight-bold has-text-centered">印刷データを送信しました</div>
          <div className="has-text-centered">
            <img src={success} width={150} />
          </div>
          <p className="modal-card-title has-text-centered has-text-white">
            受付番号: <strong className="is-size-1 has-text-white">{props.reference}</strong>
          </p>
          <div className="is-size-6 mt-5 mx-0">
            <div className="has-text-weight-bold">
              印刷後、プッシュ通知でお知らせします。
              <br />
              <br />
              <div>
                トップページの
                <span className="icon is-medium inline-icon" style={{ verticalAlign: 'sub' }}>
                  <span className="fa-stack">
                    <i className="fas fa-circle fa-stack-2x" style={{ color: '#09b4c6', fontSize: '0.75rem' }}></i>
                    <i className="fas fa-circle fa-stack-2x has-text-white" style={{ fontSize: '0.65rem' }}></i>
                    <i className="fas fa-list fa-stack-1x" style={{ color: '#09b4c6', fontSize: '0.65rem' }}></i>
                  </span>
                </span>
                から受付番号をご提示ください。
              </div>
            </div>
          </div>
          <div className="has-text-centered mt-5">
            <Link to="/">
              <button
                className="button is-medium has-text-white has-text-weight-bold"
                style={{ background: '#09b4c6' }}
                onClick={props.onClose}
              >
                とじる
              </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PrintResult
