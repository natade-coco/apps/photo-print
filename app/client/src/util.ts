import moment from 'moment'
import { HEIGHT, WIDTH } from './components/Frame'
import Compressor from 'compressorjs'
import Cropper from 'cropperjs'
const MAX = 3000
const LIMIT_COMPRESS_COUNT = 2
const LIMIT_RESIZED_IMAGE_SIZE = 5 * 1000 * 1000
export const parseDate = (date: string) => {
  return moment(date).utc(true).local().format('YYYY/MM/DD HH:mm')
}

export const resizeImage = (
  file: File | Blob,
  successCb: (result: string) => void,
  failCb: () => void,
  quality = 1,
  count = 0
) => {
  const reader = new FileReader()
  const onSuccess = () => {
    reader.readAsDataURL(file)
    reader.onload = () => {
      const base64 = reader.result
      successCb(base64.toString())
    }
  }
  if (file.size < LIMIT_RESIZED_IMAGE_SIZE) {
    onSuccess()
    return
  }

  new Compressor(file, {
    quality,
    maxHeight: MAX,
    maxWidth: MAX,
    convertSize: 10000000,
    success: (result) => {
      if (count < LIMIT_COMPRESS_COUNT) {
        resizeImage(result, successCb, failCb, quality - 0.1, count + 1)
      } else {
        onSuccess()
      }
    },
    error: (err) => {
      console.log(err)
      failCb()
    }
  })
}

// boxData: Cropper.CropBoxData, canvasData: Cropper.CanvasData,
export const cropper = (image: HTMLImageElement, cb: (base64: string) => void) => {
  const cropper = new Cropper(image, {
    ready: () => {
      const containerData = cropper.getContainerData()
      cropper
        .setCropBoxData({
          height: containerData.height * 0.85
        })
        .setCropBoxData({
          left: (containerData.width - cropper.getCropBoxData().width) / 2
        })
        .setCropBoxData({
          top: (containerData.height - cropper.getCropBoxData().height) / 2
        })
      const boxData = cropper.getCropBoxData()
      let originCropper: Cropper
      if (image.height / image.width > HEIGHT / WIDTH) {
        originCropper = cropper
          .setCanvasData({
            width: boxData.height * (HEIGHT / WIDTH)
          })
          .setCanvasData({
            top: (containerData.height - cropper.getCanvasData().height) / 2,
            left: (containerData.width - cropper.getCanvasData().width) / 2
          })
      } else {
        originCropper = cropper
          .setCanvasData({
            height: boxData.height,
            top: boxData.top
          })
          .setCanvasData({ left: (containerData.width - cropper.getCanvasData().width) / 2 })
      }
      const base64 = originCropper
        .getCroppedCanvas({
          height: HEIGHT,
          width: WIDTH,
          fillColor: 'black'
        })
        .toDataURL('image/jpg', 1.0)
      cb(base64)
      cropper.destroy()
    },
    aspectRatio: WIDTH / HEIGHT,
    viewMode: 0,
    checkOrientation: false
  })
}

export const verticallyCropper = (image: HTMLImageElement, cb: (base64: string) => void) => {
  const cropper = new Cropper(image, {
    ready: () => {
      const containerData = cropper.getContainerData()
      cropper
        .setCropBoxData({
          height: containerData.height * 0.85
        })
        .setCropBoxData({
          left: (containerData.width - cropper.getCropBoxData().width) / 2
        })
        .setCropBoxData({
          top: (containerData.height - cropper.getCropBoxData().height) / 2
        })
      const boxData = cropper.getCropBoxData()
      let originCropper: Cropper
      if (image.height / image.width > HEIGHT / WIDTH) {
        originCropper = cropper
          .setCanvasData({
            width: boxData.width,
            left: boxData.left
          })
          .setCanvasData({ top: (containerData.height - cropper.getCanvasData().height) / 2 })
      } else {
        originCropper = cropper
          .setCanvasData({
            height: boxData.height,
            top: boxData.top
          })
          .setCanvasData({ left: (containerData.width - cropper.getCanvasData().width) / 2 })
      }
      const base64 = originCropper
        .getCroppedCanvas({
          height: 1407,
          width: 1052,
          fillColor: 'black'
        })
        .toDataURL('image/jpg', 1.0)
      cb(base64)
      cropper.destroy()
    },
    aspectRatio: 1052 / 1407,
    viewMode: 0,
    checkOrientation: false
  })
}

export const cropImage = (
  image: HTMLImageElement,
  cb: (base64: string) => void,
  containerData: Cropper.ContainerData,
  canvasData: Partial<Cropper.CanvasData>,
  isVertically = false
) => {
  if (isVertically) {
    console.log('isVertically')
    const cropper = new Cropper(image, {
      ready: () => {
        const originContainer = cropper.getContainerData()
        const heightRation = originContainer.height / containerData.height
        const widthRation = originContainer.width / containerData.width
        const base64 = cropper
          // .setCropBoxData({ height: containerData.width * 0.85 })
          // .setCropBoxData({
          //   left: (containerData.width - cropper.getCropBoxData().width) / 2,
          // })
          // .setCropBoxData({
          //   top: (containerData.height - cropper.getCropBoxData().height) / 2,
          // })
          .setCanvasData({ height: canvasData.height })
          .getCroppedCanvas({
            height: HEIGHT,
            width: WIDTH,
            fillColor: 'black'
          })
          .toDataURL('image/jpg', 1.0)
        cb(base64)
        // cropper.destroy()
      },
      aspectRatio: WIDTH / HEIGHT,
      viewMode: 0,
      autoCropArea: 0.85,
      checkOrientation: false
    })
  } else {
    const cropper = new Cropper(image, {
      ready: () => {
        const base64 = cropper
          // .setCropBoxData({ height: containerData.height * 0.85 })
          // .setCropBoxData({
          //   left: (containerData.width - cropper.getCropBoxData().width) / 2,
          // })
          // .setCropBoxData({
          //   top: (containerData.height - cropper.getCropBoxData().height) / 2,
          // })
          // .setCanvasData(canvasData)
          .getCroppedCanvas({
            height: WIDTH,
            width: HEIGHT,
            fillColor: 'black'
          })
          .toDataURL('image/jpg', 1.0)
        cb(base64)
        // cropper.destroy()
      },
      aspectRatio: 1052 / 1407,
      viewMode: 0,
      checkOrientation: false,
      autoCropArea: 0.85,
      minContainerWidth: containerData.width,
      minCanvasHeight: containerData.height
    })
  }
}

export const drawImage = (reference: number, target: HTMLCanvasElement, hasNumber = true, isVertically = false) => {
  const print = document.createElement('canvas') // 受付番号描画用canvas生成
  if (isVertically) {
    print.width = HEIGHT
    print.height = WIDTH
  } else {
    print.width = WIDTH
    print.height = HEIGHT
  }
  const ctx = print.getContext('2d')
  ctx.globalAlpha = 1.0
  ctx.drawImage(target, 0, 0)
  // 受付番号背景描画
  if (hasNumber) {
    ctx.fillStyle = 'white'
    ctx.globalAlpha = 0.7
    const rectWidth = 45 + 12 * (`${reference}`.length - 3)
    ctx.fillRect(0, target.height - 24, rectWidth, 24)
    // 受付番号描画
    ctx.fillStyle = 'black'
    ctx.globalAlpha = 0.7
    ctx.font = '16pt Arial'
    ctx.fillText(`${reference}`, 5, target.height - 4)
  }
  const base64 = print.toDataURL('image/jpeg', 1.0).split(',')[1]
  return base64
}
