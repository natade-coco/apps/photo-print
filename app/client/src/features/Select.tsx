import React, { useEffect, useRef, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Selector } from '../components/Selector'
import SizeLimit from '../components/SizeLimit'
import { navigate } from 'gatsby'

import AppImage from '../images/img-top-fuchi.png'
import { resizeImage } from '../util'
import Error from '../components/Error'
import { ImageIndexedDB } from '../services/indexedDB'
import { PrintListBtn } from '../components/PrintListBtn'
import { getPrintdocs, printdocSelector } from '../slices/printdoc'
import { AppDispatch } from '../app/store'
import { isLoadingSelector, printCountSelector, sessionIdSelector } from '../slices/app'
import { printLimitSelector } from '../slices/setting'

export const FILE_SIZE_LIMIT = 50 * 1000 * 1000 // 50MB
const storage = new ImageIndexedDB()
const SelectImage = (props: any) => {
  const dispatch: AppDispatch = useDispatch()
  const sessionId = useSelector(sessionIdSelector)
  const isLoading = useSelector(isLoadingSelector)
  const printLimit = useSelector(printLimitSelector)
  const printCount = useSelector(printCountSelector)
  const printDocs = useSelector(printdocSelector.selectAll)

  const [showErrorModal, setShowErrorModal] = useState(false)
  const [showSizeModal, setShowSizeModal] = useState(false)

  const onClickedSelector = () => {
    const inputElement = document.getElementById('selectedImage')
    inputElement.click()
  }
  useEffect(() => {}, [])

  const onSelectedImage = (selected: any) => {
    const successCb = (base64: string) => {
      storage.setImage(base64, () => {
        navigate('/preview')
      })
    }
    const failCb = () => {
      setShowErrorModal(true)
    }
    if (selected.target?.files.length > 0) {
      const file = selected.target.files[0]
      if (file.size > FILE_SIZE_LIMIT) {
        setShowSizeModal(true)
      } else {
        resizeImage(file, successCb, failCb)
      }
    }
  }
  const onCloseSizeLimit = () => {
    setShowSizeModal(false)
  }

  const errorTitle = '画像の圧縮に失敗しました'
  const errorMessage = '他の写真を選択してください。'
  const onCloseError = () => {
    setShowErrorModal(false)
  }
  const nonReadPrintItemCnt = printDocs.filter((item) => !item.isRead && item.status === 'completed').length

  return (
    <div className="is-flex is-flex-direction-column" style={{ minHeight: '100vh' }}>
      <div style={{ position: 'absolute', top: '2vh', right: '2vh', zIndex: 2 }}>
        <PrintListBtn count={nonReadPrintItemCnt} />
      </div>
      <div
        className="has-text-centered is-flex is-justify-content-center is-flex-direction-column"
        style={{ minHeight: '70vh' }}
      >
        <div style={{ marginTop: '10vh' }}>
          <img style={{ width: 'auto', height: 'auto', maxWidth: '100%', maxHeight: '45vh' }} src={AppImage} />
        </div>
        <div className="has-text-white is-relative is-size-4" style={{ minHeight: '10vh' }}>
          <div className="catch-phrase">
            思い出の写真を
            <br />
            いますぐかんたんプリント
          </div>
        </div>
      </div>
      <div
        className="is-flex is-justify-content-space-evenly is-flex-direction-column is-flex-grow-1"
        style={{ minHeight: '20vh' }}
      >
        <Selector
          isLoading={isLoading}
          onClickSelector={onClickedSelector}
          onSelectedImage={onSelectedImage}
          remainCount={printLimit - printCount}
        />
      </div>
      <SizeLimit showNotice={showSizeModal} onClose={onCloseSizeLimit} />
      <Error message={errorMessage} title={errorTitle} onClose={onCloseError} showNotice={showErrorModal} />
    </div>
  )
}

export default SelectImage
