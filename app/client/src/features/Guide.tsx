import * as React from 'react'
import { guide } from '../docs/guide'

const Guide = (props: any) => {
  return <div dangerouslySetInnerHTML={{ __html: guide }}></div>
}

export default Guide
