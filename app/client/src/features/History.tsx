import React, { useEffect } from 'react'
import PrintList from '../components/PrintList'
import { useDispatch, useSelector } from 'react-redux'
import { navigate } from 'gatsby-link'
import leftArrow from '../images/left-arrow.svg'
import { AppDispatch } from '../app/store'
import { getPrintdocs, printdocSelector, updatePrintdocRead } from '../slices/printdoc'
import { isLoadingSelector, userSelector } from '../slices/app'

const Histroy = ({ location }: any) => {
  const dispatch: AppDispatch = useDispatch()
  const printdocs = useSelector(printdocSelector.selectAll)
  const userId = useSelector(userSelector)
  const isLoading = useSelector(isLoadingSelector)

  useEffect(() => {
    userId &&
      dispatch(getPrintdocs(userId))
        .unwrap()
        .then((item) => {
          const updates = item.filter((item) => !item.isRead && item.status === 'completed').map((target) => target.id)
          if (updates.length > 0) {
            dispatch(updatePrintdocRead(updates))
          }
        })
  }, [])

  return (
    <div className="container pb-3" style={{ minHeight: '100vh', overflowY: 'scroll' }}>
      <Header />
      <PrintList isLoading={isLoading} histories={printdocs} />
    </div>
  )
}

export default Histroy

const Header = () => (
  <div className="columns is-mobile m-0">
    <div className="column is-2 is-narrow is-flex is-align-items-center">
      <div>
        <div
          style={{
            display: 'flex',
            height: '2rem',
            width: '2rem',
            border: '2px solid #fff',
            borderRadius: '50%',
            justifyContent: 'center',
            flexDirection: 'column'
          }}
          onClick={() => {
            navigate('/')
          }}
        >
          <img className="is-rounded" src={leftArrow} style={{ height: '1.4rem', color: '#fff' }} />
        </div>
      </div>
    </div>
    <div
      className="column is-8 is-narrow has-text-white has-text-centered has-text-weight-bold is-size-5"
      style={{ textShadow: '0px 1px 3px #00000055' }}
    >
      プリント状況
    </div>
  </div>
)
