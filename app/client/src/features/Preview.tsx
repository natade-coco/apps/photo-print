import { navigate } from 'gatsby-link'
import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import ConfirmPayment from '../components/ConfirmPayment'
import Frame, { HEIGHT, WIDTH } from '../components/Frame'
import PrintResult from '../components/PrintResult'
import SizeLimit from '../components/SizeLimit'
import { DocumentStorage } from '../services/storage'
import { cropper, drawImage, verticallyCropper } from '../util'
import { ReactCropperElement } from 'react-cropper'
import { ImageIndexedDB } from '../services/indexedDB'
import EditMenu, { EditMode } from '../components/EditMenu'
import CropperComponent from '../components/Cropper'
import leftArrow from '../images/left-arrow.svg'
import PhotoEditConfirm from '../components/PhotoEditConfirm'
import PrintBtn from '../components/PrintBtn'
import { AppDispatch } from '../app/store'
import { getPhotoframes, photoframeSelector } from '../slices/photoframe'
import {
  envSelector,
  incrementPrintCnt,
  isLoadingSelector,
  printCountSelector,
  printIdSelector,
  printTotalSelector,
  userSelector
} from '../slices/app'
import {
  hasNumberSelector,
  isPaidSelector,
  printAmountSelector,
  printLimitSelector,
  stripeAccountSelector
} from '../slices/setting'
import { CanvasData } from '../entities/app'
import { createPrintdoc, getPrintdocs, printdocSelector, removePrintdoc, updatePrintdocState } from '../slices/printdoc'

const storage = new DocumentStorage()
const imageStorage = new ImageIndexedDB()

const Preview = (props: any) => {
  const cropperRef = useRef<ReactCropperElement>()
  const dispatch: AppDispatch = useDispatch()
  const photoframes = useSelector(photoframeSelector.selectAll)
  const env = useSelector(envSelector)
  const isPaid = useSelector(isPaidSelector)
  const userId = useSelector(userSelector)
  const hasNumber = useSelector(hasNumberSelector)
  const printId = useSelector(printIdSelector)
  const printAmount = useSelector(printAmountSelector)
  const printCount = useSelector(printCountSelector)
  const printLimit = useSelector(printLimitSelector)
  const stripeAccount = useSelector(stripeAccountSelector)
  const isLoading = useSelector(isLoadingSelector)
  const printdocs = useSelector(printdocSelector.selectAll)
  const printTotal = useSelector(printTotalSelector)

  const [selectedFrame, setSelectedFrame] = useState(0)
  const [cropCanvasData, setCropCanvasData] = useState<CanvasData>(null)
  const [croppedImage, setCroppedImage] = useState('')
  const [croppedImageVertically, setCroppedImageVertically] = useState('')
  const [isInitializedCropper, setIsInitializedCropper] = useState(false)
  const [printResultModalState, setPrintResultModalState] = useState(false)
  const [sizeLimitModalState, setSizeLimitModalState] = useState(false)
  const [confirmPaymentModalState, setConfirmPaymentModalState] = useState(false)
  const [image, setImage] = useState('')
  const [resultRef, setResultRef] = useState(100)
  useEffect(() => {
    imageStorage.getImage((selectedImage) => {
      if (!selectedImage) {
        navigate('/')
      } else {
        setImage(selectedImage)
        dispatch(getPhotoframes())
      }
    })
    const image = document.getElementById('test-image') as HTMLImageElement
    image.onload = () => {
      const cb1 = (base64: string) => {
        setCroppedImage(base64)
      }
      cropper(image, cb1)
    }
    const image_v = document.getElementById('test-image_vertically') as HTMLImageElement
    image_v.onload = () => {
      const cb2 = (base64: string) => {
        setCroppedImageVertically(base64)
      }
      verticallyCropper(image_v, cb2)
    }
  }, [])

  const [isRenderingImg, setRenderingState] = useState(false)

  const ref = useRef<HTMLElement>(null)

  const onFinishDraw = () => {
    const target = document.getElementById(`canvas-${selectedFrame}`) as HTMLCanvasElement
    const preview = document.getElementById('canvas-preview') as HTMLCanvasElement
    const slider = document.querySelector('.slick-slider') as HTMLElement
    preview.width = photoframes[selectedFrame]?.direction === 'vertically' ? HEIGHT : WIDTH
    preview.height = photoframes[selectedFrame]?.direction === 'vertically' ? WIDTH : HEIGHT
    if (preview && target && slider) {
      const ctx = preview.getContext('2d')
      ctx.drawImage(target, 0, 0)
      if (ref.current && !initFocus) {
        setFocus(true)
        ref.current.style.width = `${target.offsetWidth + 6}px`
        ref.current.style.height = `${slider.offsetHeight}px`
        ref.current.style.display = 'block'

        if (!photoframes.every((item) => item.direction === 'horizontally')) {
          photoframes.map((item, index) => {
            if (item.direction === 'horizontally') {
              const _target = document.querySelector(`div > #canvas-${index}`) as HTMLCanvasElement
              const parent = _target?.parentNode as HTMLElement
              if (parent) {
                parent.style.height = slider.offsetHeight - 6 + 'px'
                parent.style.display = 'flex'
                parent.style.flexDirection = 'column'
                parent.style.justifyContent = 'space-around'
              }
            }
          })
        }
      }
    }
    const frameArea = document.getElementById('frame-area')
    setHeight(window.innerHeight - frameArea.offsetHeight)
    setEditMode('frame')
    setRenderingState(false)
  }
  const [initFocus, setFocus] = useState(false)

  const onChangeFrameNum = (num: number) => {
    const frameImage = document.getElementById('frame-on-cropper') as HTMLImageElement
    if (cropperRef?.current?.cropper) {
      const cropper = cropperRef.current.cropper
      const isVertically = photoframes[num].direction === 'vertically'
      cropper.setAspectRatio(isVertically ? HEIGHT / WIDTH : WIDTH / HEIGHT)
      const containerData = cropper.getContainerData()
      const cropboxData = cropper.getCropBoxData()
      cropper
        .setCropBoxData({
          height: containerData.height * 0.85
        })
        .setCropBoxData({
          left: (containerData.width - cropper.getCropBoxData().width) / 2
        })
        .setCropBoxData({
          top: (containerData.height - cropper.getCropBoxData().height) / 2
        })
      if (frameImage) {
        const cropperBox = document.querySelector('#image-edit-cropper .cropper-crop-box') as HTMLElement
        frameImage.style.width = cropperBox.style.width
        frameImage.style.transform = cropperBox.style.transform
      }
    }
    setSelectedFrame(num)
  }

  const [height, setHeight] = useState(0)

  const onClickEditBtn = () => {
    // dispatch(showImageEditor({}))
  }

  const onClickPrint = () => {
    setCropCanvasData({})
    const reference = getRefence()
    const target = document.getElementById(`canvas-${selectedFrame}`) as HTMLCanvasElement
    const isVertically = photoframes[selectedFrame].direction === 'vertically'
    const doc = drawImage(reference, target, hasNumber, isVertically)
    const frameId = photoframes[selectedFrame].id
    if (isPaid && env === 'stg') {
      const obj = { doc, did: userId, reference, frameId, isVertically, status: 'pending', isPaid: true } as const
      dispatch(createPrintdoc(obj))
        .unwrap()
        .then((order) => {
          setConfirmPaymentModalState(true)
        })
    } else {
      const obj = { doc, did: userId, reference, frameId, isVertically }
      dispatch(createPrintdoc(obj))
        .unwrap()
        .then((order) => {
          dispatch(incrementPrintCnt({ id: printId, cnt: printCount + 1, total: printTotal + 1 }))
          setPrintResultModalState(true)
          setResultRef(order.reference)
        })
    }
  }
  const getRefence = () => {
    const second = printdocs.length > 0 ? Number(String(printdocs[0].reference).slice(String(printId).length)) + 1 : 0
    const reference = Number(printId + ('00' + second.toString()).slice(-2))
    return reference
  }

  const onClickCloseResult = () => {
    setPrintResultModalState(true)
    imageStorage.clearImage(() => {
      navigate('/')
    })
  }

  const removeDoc = () => {
    const { id, doc } = storage.getDocument()
    if (id) {
      dispatch(removePrintdoc({ id: Number(id), doc: Number(doc) }))
      setConfirmPaymentModalState(false)
    }
  }

  const onErrorPayment = (reason: any) => {
    removeDoc()
  }

  const onCompletedPayment = (txId: string, state: string, chargeId: string, receiptUrl: string): boolean => {
    setConfirmPaymentModalState(false)
    setCropCanvasData({})
    const { id } = storage.getDocument()
    dispatch(updatePrintdocState({ id: Number(id), status: 'uploaded' }))
    dispatch(incrementPrintCnt({ id: printId, cnt: printCount + 1, total: printTotal + 1 }))
    return true
  }

  const onClosePayment = () => {
    removeDoc()
    setConfirmPaymentModalState(false)
  }

  const onCloseSizeLimit = () => setSizeLimitModalState(false)
  const frame = photoframes[selectedFrame]?.frame || ''

  const [editMode, setEditMode] = useState<EditMode>('frame')
  const onClickMenu = (mode: EditMode) => {
    if (mode === 'adjust') {
      setIsInitializedCropper(true)
      setEditable(true)
    } else {
      setEditable(false)
    }
    setEditMode(mode)
  }

  const [isModified, setModified] = useState(false)
  const onModifyCropper = () => {
    setModified(true)
  }
  useEffect(() => {
    const frameArea = document.getElementById('frame-area')
    setHeight(window.innerHeight - frameArea.offsetHeight)
  }, [editMode])

  const onConfirmEditedPhoto = () => {
    setRenderingState(true)
    setEditable(false)
    setModified(false)
    const cropper = cropperRef.current!.cropper
    const isVertically = photoframes[selectedFrame].direction === 'vertically'
    const base64 = cropper
      .getCroppedCanvas(isVertically ? { width: WIDTH, height: HEIGHT } : { width: HEIGHT, height: WIDTH })
      .toDataURL('image/jpeg', 1.0)
    const containerData = cropper.getContainerData()
    const cropBoxData = cropper.getCropBoxData()
    const canvasData = cropper.getCanvasData()
    const rotate = cropper.getData().rotate
    const scaleX = cropper.getData().scaleX
    const scaleY = cropper.getData().scaleY

    setCropCanvasData({ ...canvasData, rotate, scaleX, scaleY })
    isVertically ? setCroppedImageVertically(base64) : setCroppedImage(base64)
    // dispatch(resetImageEditor({}))
    const base64_ex = cropper
      .setAspectRatio(isVertically ? WIDTH / HEIGHT : HEIGHT / WIDTH)
      .setCropBoxData({
        height: cropBoxData.height
      })
      .setCropBoxData({
        left: (containerData.width - cropper.getCropBoxData().width) / 2
      })
      .setCropBoxData({
        top: (containerData.height - cropper.getCropBoxData().height) / 2
      })
      .getCroppedCanvas(!isVertically ? { width: WIDTH, height: HEIGHT } : { width: HEIGHT, height: WIDTH })
      .toDataURL('image/jpeg', 1.0)
    isVertically ? setCroppedImage(base64_ex) : setCroppedImageVertically(base64_ex)
    cropper
      .setAspectRatio(!isVertically ? WIDTH / HEIGHT : HEIGHT / WIDTH)
      .setCropBoxData({
        height: cropBoxData.height
      })
      .setCropBoxData({
        left: (containerData.width - cropper.getCropBoxData().width) / 2
      })
      .setCropBoxData({
        top: (containerData.height - cropper.getCropBoxData().height) / 2
      })
  }

  const onCancelEditedPhoto = () => {
    const cropper = cropperRef.current!.cropper
    if (cropCanvasData) {
      const { height, left, top, width, rotate, scaleX, scaleY } = cropCanvasData
      cropper.scale(scaleX, scaleY).rotateTo(rotate).setCanvasData({ height, left, top, width })
    } else {
      cropper.rotateTo(0).scale(1, 1)
      const containerData = cropper.getContainerData()
      const cropBoxData = cropper.getCropBoxData()
      const imageData = cropper.getCanvasData()
      const isVertically = cropBoxData.height > cropBoxData.width
      if (isVertically) {
        if (imageData.height / imageData.width > HEIGHT / WIDTH) {
          cropper
            .setCanvasData({
              width: cropBoxData.width,
              left: cropBoxData.left
            })
            .setCanvasData({ top: (containerData.height - cropper.getCanvasData().height) / 2 })
        } else {
          cropper
            .setCanvasData({
              height: cropBoxData.height,
              top: cropBoxData.top
            })
            .setCanvasData({ left: (containerData.width - cropper.getCanvasData().width) / 2 })
        }
      } else {
        if (imageData.height / imageData.width > HEIGHT / WIDTH) {
          cropper
            .setCanvasData({
              width: cropBoxData.height * (HEIGHT / WIDTH)
            })
            .setCanvasData({
              top: (containerData.height - cropper.getCanvasData().height) / 2,
              left: (containerData.width - cropper.getCanvasData().width) / 2
            })
        } else {
          cropper
            .setCanvasData({
              height: cropBoxData.height,
              top: cropBoxData.top
            })
            .setCanvasData({ left: (containerData.width - cropper.getCanvasData().width) / 2 })
        }
      }
    }
    setModified(false)
  }

  const [isEditable, setEditable] = useState(false)

  return (
    <div className="is-loading">
      <div className="bg-filter" />
      <div id="frame-area" style={{ minHeight: '69vh' }}>
        <div style={{ padding: '0.7rem 0 0 0.7rem' }}>
          <div
            style={{
              display: 'flex',
              height: '2rem',
              width: '2rem',
              border: '1px solid #fff',
              borderRadius: '50%',
              justifyContent: 'center',
              flexDirection: 'column'
            }}
            onClick={() => {
              navigate('/')
            }}
          >
            <img className="is-rounded" src={leftArrow} style={{ height: '1.4rem' }} />
          </div>
        </div>
        <EditMenu isModified={isModified} mode={editMode} onClickMenu={onClickMenu} />
        <div>
          <div style={{ opacity: isRenderingImg ? 0 : 1 }}>
            <div style={{ display: `${editMode === 'frame' ? 'block' : 'none'}`, position: 'relative' }}>
              <Frame
                selectedFrameNum={selectedFrame}
                photoframes={photoframes}
                isLoading={isLoading}
                onChangeFrameNum={onChangeFrameNum}
                selectedImage={croppedImage}
                onClickEditImageBtn={onClickEditBtn}
                verticallySelectedImage={croppedImageVertically}
                onFinishDraw={onFinishDraw}
                focusRef={ref}
              />
            </div>
            {isInitializedCropper && (
              <div style={{ display: `${editMode === 'adjust' ? 'block' : 'none'}` }}>
                <CropperComponent
                  selectedFrame={frame}
                  isEditable={isEditable}
                  cropperRef={cropperRef}
                  cropperOption={cropCanvasData}
                  src={image}
                  isVertically={photoframes[selectedFrame]?.direction === 'vertically'}
                  onModify={onModifyCropper}
                />
              </div>
            )}
          </div>
        </div>
      </div>
      <div style={{ display: 'none' }}>
        <img id="test-image" src={image} />
        <img id="test-image_vertically" src={image} />
      </div>
      <div
        className="is-relative is-flex is-justify-content-space-evenly	is-flex-direction-column"
        style={{ minHeight: height }}
      >
        {isModified ? (
          <PhotoEditConfirm
            onClickCancel={onCancelEditedPhoto}
            onClickConfirm={onConfirmEditedPhoto}
            isLoading={isRenderingImg}
          />
        ) : (
          <>
            <PrintBtn
              amount={printAmount}
              count={printLimit - printCount}
              test={isPaid && env === 'stg'}
              isDisabled={(!(photoframes.length > 0) || printLimit - printCount < 1) && !isPaid}
              isLoading={isLoading}
              onClickPrint={onClickPrint}
            />
            <div className="has-text-white has-text-weight-bold" style={{ textAlign: 'center', fontSize: '0.8rem' }}>
              {props.test
                ? `プリント料金: ${printAmount}円/枚`
                : `今日のプリント回数: あと${printLimit - printCount}回`}
            </div>
          </>
        )}
      </div>
      <PrintResult showResult={printResultModalState} reference={resultRef} onClose={onClickCloseResult} />
      <SizeLimit showNotice={sizeLimitModalState} onClose={onCloseSizeLimit} />
      {isPaid && env === 'stg' && (
        <ConfirmPayment
          onClose={onClosePayment}
          account={stripeAccount}
          showPayment={confirmPaymentModalState}
          onCompleted={onCompletedPayment}
          amount={printAmount}
          onError={onErrorPayment}
        />
      )}
    </div>
  )
}
export default Preview
