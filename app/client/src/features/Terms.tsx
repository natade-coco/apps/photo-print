import * as React from 'react'
import { terms } from '../docs/terms'

const Terms = (props: any) => {
  return <div dangerouslySetInnerHTML={{ __html: terms }}></div>
}

export default Terms
