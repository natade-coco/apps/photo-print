import * as React from 'react'
import { privacy } from '../docs/privacy'

const Privacy = (props: any) => {
  return <div dangerouslySetInnerHTML={{ __html: privacy }}></div>
}

export default Privacy
