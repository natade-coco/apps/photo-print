import React, { useEffect, useState } from 'react'

import Layout from '../components/Layout'
import Router from '../components/Router'
import Footer from '../components/Footer'

import Select from '../features/Select'
import Preview from '../features/Preview'
import History from '../features/History'
import Terms from '../features/Terms'
import './index.scss'
import { useDispatch, useSelector } from 'react-redux'
import { HashLoader } from 'react-spinners'
import Guide from '../features/Guide'
import Privacy from '../features/Privacy'
import { DocumentStorage } from '../services/storage'
import { navigate } from 'gatsby-link'
import Timeout from '../components/Timeout'
import { AppDispatch } from '../app/store'
import { appStart, errorSelector, sessionIdSelector, showErrorSelector } from '../slices/app'
import { Printdoc } from '../entities/printdoc'
import AppService from '../services/server/app'
import { getPrintdocs, updatePrintdocState } from '../slices/printdoc'
import { getSetting } from '../slices/setting'
const appService = new AppService()

const Page = () => {
  const dispatch: AppDispatch = useDispatch()
  const [isAppReady, setIsAppReady] = useState(false)
  const sessionId = useSelector(sessionIdSelector)
  const showError = useSelector(showErrorSelector)
  const error = useSelector(errorSelector)

  useEffect(() => {
    console.log(sessionId)
    if (!sessionId) {
      const detectedCallback = (printDoc?: Printdoc) => {
        if (printDoc) return
        dispatch(updatePrintdocState({ id: printDoc.id, status: printDoc.status }))
      }
      appService.subscribeUpdatePrintState(detectedCallback)
      dispatch(appStart())
        .unwrap()
        .then(({ user }) => {
          dispatch(getSetting()).then(() => {
            setIsAppReady(true)
            const storage = new DocumentStorage()
            const document = storage.getDocument()
            if (document) {
              navigate('/preview')
              return
            }
            dispatch(getPrintdocs(user.id))
          })
        })
    }
  }, [])
  const onReload = () => {
    window.location.reload()
  }
  return (
    <>
      {isAppReady ? (
        <Layout>
          <div className="main-content"></div>
          <div style={{ minHeight: '100vh' }}>
            <Router>
              <Select path="/" />
              <Preview path="preview" />
              <History path="history" />
              <Guide path="guide" />
              <Terms path="terms" />
              <Privacy path="privacy" />
            </Router>
          </div>
          <Timeout onClose={onReload} showNotice={showError} error={error} />
          <Footer />
        </Layout>
      ) : (
        <Loader />
      )}
    </>
  )
}
export default Page

const Loader = () => (
  <div className="hero is-fullheight">
    <div className="hero-body">
      <div className="container" style={{ flex: '0 0 auto' }}>
        <HashLoader size={100} color={'#09b4c6'} loading />
      </div>
    </div>
  </div>
)
