const activeEnv = process.env.GATSBY_ACTIVE_ENV || process.env.NODE_ENV || "development"
console.log(`Using environment config: '${activeEnv}'`)
require("dotenv").config({
  path: `.env.${activeEnv}`,
})
console.log("process.env.SENTRY_DSN_URL = ", process.env.CLIENT_SENTRY_DSN_URL)

module.exports = {
  siteMetadata: {
    title: `photo-print`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-scss-typescript`,
    {
      resolve: "gatsby-plugin-sentry",
      options: {
        dsn: process.env.CLIENT_SENTRY_DSN_URL,
        // Optional settings, see https://docs.sentry.io/clients/node/config/#optional-settings
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      }
    }
  ],
}
