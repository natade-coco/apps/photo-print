#!/bin/sh
set -e

npm run datastore:setup && npm run datastore:seed && npm run start