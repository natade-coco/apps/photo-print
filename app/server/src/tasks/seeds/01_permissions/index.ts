import { Directus, TypeMap } from '@directus/sdk';

import permissions from './permissions.json';

const collection = { name: 'permissions', data: permissions };

const targetCollectionNames = ['directus_files'];

export async function seed(client: Directus<TypeMap>) {
  const check = await client.permissions
    .readByQuery({
      filter: {
        collection: {
          _in: targetCollectionNames
        }
      }
    })
    .catch(console.log);
  if (check && check.data) {
    check.data.forEach((item) => {
      client.permissions.deleteOne((item as any).id);
    });
  }
  await client.permissions.createMany(collection.data).catch(console.log);
  console.log(collection.name, 'items created');
}

export async function backup(client: Directus<TypeMap>, path: string) {
  var fs = require('fs');

  const check = await client.permissions
    .readByQuery({
      filter: {
        collection: {
          _in: targetCollectionNames
        }
      }
    })
    .catch(console.log);
  if (check && check.data) {
    console.log(collection.name + ' exists');
    const filtered = check.data.map((item: any) => {
      delete item.id;
      return item;
    });
    fs.writeFileSync(path + '/' + collection.name + '.json', JSON.stringify(filtered, null, '    '));
    console.log(collection.name, 'items backup');
  }
}
