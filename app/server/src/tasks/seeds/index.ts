import { Directus, TypeMap } from '@directus/sdk';

import * as Permissions from './01_permissions';
import * as Webhooks from './02_webhooks';
import * as Settings from './03_settings';
import * as PhotoFrames from './04_photoframes';
import * as Presets from './05_presets';

export function seed(client: Directus<TypeMap>, internalURL: string) {
  Permissions.seed(client);
  Webhooks.seed(client, internalURL);
  Settings.seed(client);
  Presets.seed(client);
  PhotoFrames.seed(client);
}

export function backup(client: Directus<TypeMap>) {
  Permissions.backup(client, './src/tasks/seeds/01_permissions');
  Webhooks.backup(client, './src/tasks/seeds/02_webhooks');
  Settings.backup(client, './src/tasks/seeds/03_settings');
  PhotoFrames.backup(client, './src/tasks/seeds/04_photoframes');
  Presets.backup(client, './src/tasks/seeds/05_presets');
}
