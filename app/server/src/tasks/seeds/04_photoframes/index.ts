import { Directus, TypeMap } from '@directus/sdk';
import settings from './photoframes.json';

const FormData = require('form-data');
import fetch from 'node-fetch';
import fs from 'fs';

const collection = {
  name: 'photoframes',
  data: settings
};

const frames = ['natadeCOCO_1.png', 'natadeCOCO_2.png', 'natadeCOCO_3.png', 'natadeCOCO_4.png'];

export async function seed(client: Directus<TypeMap>) {
  console.log('checking if settings exists...');
  const check = await client.items(collection.name).readByQuery().catch(console.log);
  if (check && check.data?.length === 0) {
    console.log(collection.name, 'collection exists');
    const data = collection.data;
    await Promise.all(
      frames.map(async (name, index) => {
        const file = fs.readFileSync(`./assets/${name}`, 'base64');
        const buffer = Buffer.from(file, 'base64');
        const formdata = new FormData();
        formdata.append('data', buffer, name);
        formdata.append('filename_disk', name);
        formdata.append('filename_download', name);
        const token = await client.auth.token;
        const requestOptions = {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${token}`
          },
          body: formdata
        };
        const url = client.url;
        const result = await fetch(`${url}/files`, requestOptions);
        const json = await result.json();
        const id = json.data.id;
        data[index].frame = id;
      })
    );
    client
      .items(collection.name)
      .createMany(data)
      .then(() => {
        console.log(collection.name, 'items created');
      })
      .catch(console.log);
  } else {
    console.log(collection.name, 'collection not exists');
  }
}

export async function backup(client: Directus<TypeMap>, path: string) {
  var fs = require('fs');

  const check = await client.items(collection.name).readByQuery().catch(console.log);
  if (check && check.data && check.data.length > 0) {
    console.log(`${collection.name} collection exists`);
    fs.writeFileSync(path + '/' + collection.name + '.json', JSON.stringify(check.data, null, 4));
    console.log(`${collection.name} items backup`);
  } else {
    console.log(`${collection.name} collection not exists`);
  }
}
