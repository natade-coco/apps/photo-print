import { Directus, TypeMap } from '@directus/sdk';

import settings from './settings.json';

const collection = { name: 'settings', data: settings };

export async function seed(client: Directus<TypeMap>) {
  const result = await client.transport.patch(collection.name, collection.data);
  console.log('items created');
}

export async function backup(client: Directus<TypeMap>, path: string) {
  var fs = require('fs');
  const check: any = await client.settings.read().catch(console.log);
  if (check && check.data && check.data.storage_asset_presets) {
    console.log(`${collection.name} collection exists`);
    fs.writeFileSync(
      path + '/' + collection.name + '.json',
      JSON.stringify({ storage_asset_presets: check.data.storage_asset_presets }, null, 4)
    );
    console.log(`${collection.name} items backup`);
  } else {
    console.log(`${collection.name} collection not exists`);
  }
}
