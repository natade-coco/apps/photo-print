import { Directus, TypeMap } from '@directus/sdk';

import * as PrintSettings from './01_printsettings';
import * as PrintCounts from './02_printcounts';
import * as PrintDocs from './03_printdocs';
import * as PhotoFrames from './04_photoframes';

export function setup(client: Directus<TypeMap>) {
  PrintSettings.setup(client);
  PrintCounts.setup(client);
  PrintDocs.setup(client);
  PhotoFrames.setup(client);
}

export function teardown(client: Directus<TypeMap>) {
  PrintSettings.teardown(client);
  PrintCounts.teardown(client);
  PrintDocs.teardown(client);
  PhotoFrames.teardown(client);
}

export function dump(client: Directus<TypeMap>) {
  PrintSettings.dump(client, './src/tasks/migrations/01_printsettings');
  PrintCounts.dump(client, './src/tasks/migrations/02_printcounts');
  PrintDocs.dump(client, './src/tasks/migrations/03_printdocs');
  PhotoFrames.dump(client, './src/tasks/migrations/04_photoframes');
}
