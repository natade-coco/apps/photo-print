import { makeClient } from './utils';
import { teardown } from '../migrations';
(async function () {
  const client = await makeClient();
  teardown(client);
})();
