import { makeClient, makeInternalURL } from './utils';
import { seed } from '../seeds';
(async function () {
  try {
    const internalURL = await makeInternalURL();
    const client = await makeClient();
    seed(client, internalURL);
  } catch (err) {
    console.log(err);
  }
})();
