import path from 'path';
import favicon from 'serve-favicon';
import compress from 'compression';
import helmet from 'helmet';
import cors from 'cors';
import * as Sentry from '@sentry/node';
import * as Tracing from '@sentry/tracing';

import { feathers } from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';
import * as express from '@feathersjs/express';
import socketio from '@feathersjs/socketio';

import history from 'connect-history-api-fallback';

import { datastore } from './datastore';
import { interval } from './datarefresh';
import { Application } from './declarations';
import logger from './logger';
import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';
import channels from './channels';
import authentication from './authentication';
// Don't remove this comment. It's needed to format import lines nicely.

const app: Application = express.default(feathers());

// Load app configuration
app.configure(configuration());
// Load from .env
import dotenv from 'dotenv';
dotenv.config();
Sentry.init({
  dsn: process.env.SERVER_SENTRY_DSN_URL,
  integrations: [new Sentry.Integrations.Http({ tracing: true }), new Tracing.Integrations.Express({ app })],
  tracesSampleRate: 1
});
console.log('process.env.SENTRY_SERVER_DSN_URL = ', process.env.SERVER_SENTRY_DSN_URL);
app.set('directus_url', process.env.DIRECTUS_URL);
app.set('directus_id', process.env.DIRECTUS_ID);
app.set('directus_secret', process.env.DIRECTUS_SECRET);
app.set('natadecoco_id', process.env.NATADECOCO_ID);
app.set('natadecoco_secret', process.env.NATADECOCO_SECRET);
app.set('node_env', process.env.NODE_ENV || 'prd');
app.set('exec_env', process.env.EXEC_ENV || 'global');

// Enable history api fallback
app.use(history());
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(compress());
app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({ extended: true, limit: '10mb' }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', express.static(app.get('public')));

app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());
app.use(Sentry.Handlers.errorHandler());

app.use(function onError(err: any, req: any, res: any, _: any) {
  // The error id is attached to `res.sentry` to be returned
  // and optionally displayed to the user for support.
  res.statusCode = 500;
  res.end(res.sentry + '\n');
});

app.configure(datastore);
app.configure(interval);
// Set up Plugins and providers
app.configure(express.rest());
app.configure(
  socketio({
    maxHttpBufferSize: 1e8
  })
);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);
// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger } as any));

app.hooks(appHooks);
export default app;
