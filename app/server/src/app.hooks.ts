// Application hooks that run for every service
// Don't remove this comment. It's needed to format import lines nicely.
import { Datastore, recreate } from './datastore';
import toSnake from 'snakecase-keys';
import toCamel from 'camelcase-keys';
import { HookContext } from '@feathersjs/feathers/lib';
import { decode, JwtPayload } from 'jsonwebtoken';

export default {
  before: {
    all: [
      async (context: HookContext) => {
        const client = Datastore();
        const token = await client.auth.token;
        if (token) {
          const decoded = decode(token) as JwtPayload;
          const exp = decoded.exp || 0;
          const timeStamp = Math.floor(Date.now() / 1000);
          console.log({ exp, timeStamp, diff: exp - timeStamp });
          if (timeStamp > exp) {
            await recreate();
          }
        } else {
          await recreate();
        }
      },
      (context: HookContext) => {
        if (context.data) {
          context.data = toSnake(context.data, { deep: true });
        }
        return context;
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [
      // change directus response to 'camelCase' from 'snake_case'
      (context: HookContext) => {
        if (context.result) {
          context.result = toCamel(context.result, { deep: true });
        }
        return context;
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      (context: HookContext) => {
        console.log(context.error);
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
