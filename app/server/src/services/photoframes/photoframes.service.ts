// Initializes the `photoframes` service on path `/photoframes`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Photoframes } from './photoframes.class';
import hooks from './photoframes.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    photoframes: Photoframes & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/photoframes', new Photoframes(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('photoframes');

  service.hooks(hooks);
}
