import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Datastore } from '../../datastore';

interface Data {}

interface ServiceOptions {}

const COLLECTION_PHOTOFRAMES = 'photoframes';

export class Photoframes implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  async find(params?: Params): Promise<Data[] | Paginated<Data> | any> {
    console.log({ params });
    const result = await Datastore()
      .items(COLLECTION_PHOTOFRAMES)
      .readByQuery(params?.query || {})
      .catch(console.log);
    const assets = result?.data?.map((item: any) => {
      const frameId = item.frame;
      const imageUrl = `${Datastore().url}/assets/${frameId}`;
      return { ...item, frame: imageUrl };
    });
    return assets;
  }

  async get(id: Id, params?: Params): Promise<Data> {
    return {
      id,
      text: `A new message with ID: ${id}!`
    };
  }

  async create(data: Data, params?: Params): Promise<Data> {
    if (Array.isArray(data)) {
      return Promise.all(data.map((current) => this.create(current, params)));
    }

    return data;
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
  async setup() {}
}
