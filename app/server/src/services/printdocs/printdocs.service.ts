// Initializes the `printdocs` service on path `/printdocs`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Printdocs } from './printdocs.class';
import hooks from './printdocs.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    printdocs: Printdocs & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/printdocs', new Printdocs(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('printdocs');

  service.hooks(hooks);
}
