import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Datastore } from '../../datastore';
import * as Sentry from '@sentry/node';

interface Data {}

interface ServiceOptions {}

const COLLECTION_PRINTDOCS = 'printdocs';

export class Printdocs implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  async find(params?: Params): Promise<Data[] | Paginated<Data> | any> {
    const result = await Datastore()
      .items(COLLECTION_PRINTDOCS)
      .readByQuery(params?.query || {});
    const data = result.data?.map((item: any) => {
      const frameId = item.doc;
      const imageUrl = `${Datastore().url}/assets/${frameId}?key=thumbnail${item.is_vertically ? '-vertically' : ''}`;
      item.doc = imageUrl;
      return item;
    });
    return data;
  }

  async get(id: Id, params?: Params): Promise<Data | any> {
    const result = await Datastore().items(COLLECTION_PRINTDOCS).readOne(id);
    return result;
  }

  async create(data: Data, params?: Params): Promise<Data | any> {
    const transaction = Sentry.startTransaction({
      op: 'post',
      name: 'PRINT DOC',
      data
    });
    const result = await Datastore().items(COLLECTION_PRINTDOCS).createOne(data);
    transaction.finish();

    return result;
  }

  async update(id: Id, data: Data, params?: Params): Promise<Data | any> {
    const result = await Datastore().items(COLLECTION_PRINTDOCS).updateOne(id, data);
    return result;
  }

  async patch(id: NullableId | Id[], data: Data, params?: Params): Promise<Data[] | any> {
    if (id instanceof Array) {
      const result = await Datastore().items(COLLECTION_PRINTDOCS).updateMany(id, data);
      return result.data;
    }
    return data;
  }

  async remove(id: Id, params?: Params): Promise<Data | any> {
    await Datastore()
      .items(COLLECTION_PRINTDOCS)
      .deleteOne(id)
      .then(
        (result: any) => {},
        (err: any) => {
          throw err;
        }
      );
    return id;
  }
  async setup() {}
}
