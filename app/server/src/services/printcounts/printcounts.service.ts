// Initializes the `printcounts` service on path `/printcounts`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Printcounts } from './printcounts.class';
import hooks from './printcounts.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    printcounts: Printcounts & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/printcounts', new Printcounts(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('printcounts');

  service.hooks(hooks);
}
