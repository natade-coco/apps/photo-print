import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Datastore } from '../../datastore';

interface Data {}

interface ServiceOptions {}

const COLLECTION_PRINTCOUNTS = 'printcounts';

export class Printcounts implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  async find(params?: Params): Promise<Data[] | Paginated<Data> | any> {
    const result = await Datastore()
      .items(COLLECTION_PRINTCOUNTS)
      .readByQuery(params?.query || {});
    return result.data;
  }

  async get(id: Id, params?: Params): Promise<Data | any> {
    const result = Datastore().items(COLLECTION_PRINTCOUNTS).readOne(id);
    return result;
  }

  async create(data: Data, params?: Params): Promise<Data | any> {
    const result = await Datastore().items(COLLECTION_PRINTCOUNTS).createOne(data);
    return result;
  }

  async update(id: string | number, data: Data, params?: Params): Promise<Data | any> {
    const result = await Datastore().items(COLLECTION_PRINTCOUNTS).updateOne(id, data);

    return result;
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }
  async setup() {}
}
