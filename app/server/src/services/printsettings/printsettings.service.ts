// Initializes the `printsettings` service on path `/printsettings`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Printsettings } from './printsettings.class';
import hooks from './printsettings.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    printsettings: Printsettings & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/printsettings', new Printsettings(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('printsettings');

  service.hooks(hooks);
}
