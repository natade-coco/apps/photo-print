import { Application } from '../declarations';
import messages from './messages/messages.service';
import printdocs from './printdocs/printdocs.service';
import printcounts from './printcounts/printcounts.service';
import printsettings from './printsettings/printsettings.service';
import photoframes from './photoframes/photoframes.service';
import files from './files/files.service';
import hooks from './hooks/hooks.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(messages);
  app.configure(printdocs);
  app.configure(printcounts);
  app.configure(printsettings);
  app.configure(photoframes);
  app.configure(files);
  app.configure(hooks);
}
