import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Datastore } from '../../datastore';
const performance = require('perf_hooks').performance;
const FormData = require('form-data');
import fetch from 'node-fetch';

interface Data {}

interface ServiceOptions {}

export class Files implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    return [];
  }

  async get(id: Id, params?: Params): Promise<Data> {
    return {
      id,
      text: `A new message with ID: ${id}!`
    };
  }

  async create(data: any, params?: Params): Promise<any> {
    const startTime = performance.now();
    const formdata = new FormData();
    const buffer = Buffer.from(data.data, 'base64');
    formdata.append('data', buffer, 'download.jpg');
    formdata.append('filename_disk', 'image.jpg');
    formdata.append('filename_download', 'download.jpg');
    const token = await Datastore().auth.token;
    const requestOptions = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`
      },
      body: formdata
    };
    const url = this.app.get('directus_url');
    const result = await fetch(`${url}/files`, requestOptions);
    console.log({ result });

    const endTime = performance.now();
    console.log('経過時間', endTime - startTime);

    const response = await result.json();

    return response.data.id;
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove(id: Id, params?: Params): Promise<Data> {
    await Datastore()
      .files.deleteOne(id)
      .then(
        (result: any) => {},
        (err: any) => {
          throw err;
        }
      );
    return { id };
  }
  async setup() {}
}
