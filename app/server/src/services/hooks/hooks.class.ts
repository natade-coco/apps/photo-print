import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Datastore } from '../../datastore';
import { Application } from '../../declarations';

interface Data {}

interface ServiceOptions {}

export class Hooks implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  async find(params?: Params): Promise<Data[] | Paginated<Data>> {
    return [];
  }

  async get(id: Id, params?: Params): Promise<Data> {
    return {
      id,
      text: `A new message with ID: ${id}!`
    };
  }

  async create(data: any, params?: Params): Promise<any> {
    console.log('hooks create', { data });
    if (data.payload && data.payload.status && data.keys?.length > 0) {
      const result = await this.app
        .service('printdocs')
        .get(data.keys[0])
        .catch((err) => console.error(err));
      console.log('hooks create', { result });
      if (data.payload.status === 'completed' && result.state === 'published') {
        const did = this.app?.get('natadecoco_id');
        this.app.service('messages').create(
          {
            recipient: result.did,
            title: 'フォトプリント',
            message: `受付番号${result.reference}の写真の印刷が完了しました。受付カウンターでアプリを開いて、受付番号をご提示ください。`,
            did
          },
          {}
        );
      }
      return result;
    } else {
      return null;
    }
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    return data;
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    return { id };
  }

  async setup() {}
}
