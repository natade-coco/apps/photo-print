import { AuthenticationService } from '@feathersjs/authentication/lib';
import { Application as ExpressFeathers } from '@feathersjs/express';
import { Files } from './services/files/files.class';
import { Hooks } from './services/hooks/hooks.class';
import { Messages } from './services/messages/messages.class';
import { Photoframes } from './services/photoframes/photoframes.class';
import { Printcounts } from './services/printcounts/printcounts.class';
import { Printdocs } from './services/printdocs/printdocs.class';
import { Printsettings } from './services/printsettings/printsettings.class';

// A mapping of service names to types. Will be extended in service files.
export interface ServiceTypes {
  '/authentication': AuthenticationService;
  '/files': Files;
  '/hooks': Hooks;
  '/messages': Messages;
  '/photoframes': Photoframes;
  '/printcounts': Printcounts;
  '/printdocs': Printdocs;
  '/printsettings': Printsettings;
}
// The application instance type that will be used everywhere else
export type Application = ExpressFeathers<ServiceTypes>;
