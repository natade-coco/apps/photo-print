import { Params } from '@feathersjs/feathers';
import { AuthenticationBaseStrategy, AuthenticationResult } from '@feathersjs/authentication';
import { SDK as Hub, ServiceType } from '@natade-coco/hub-sdk';
import moment from 'moment';
import { recreate } from './datastore';

export class NatadeCOCOStrategy extends AuthenticationBaseStrategy {
  verifyConfiguration() {
    const config = this.configuration;

    ['tokenField'].forEach((prop) => {
      if (typeof config[prop] !== 'string') {
        throw new Error(`'${this.name}' authentication strategy requires a '${prop}' setting`);
      }
    });
  }

  get configuration() {
    const authConfig = this.authentication?.configuration;
    const config = super.configuration || {};

    return {
      service: authConfig.service,
      entity: authConfig.entity,
      entityId: authConfig.entityId,
      errorMessage: 'Invalid token',
      tokenField: config.tokenField,
      ...config
    };
  }

  async verifyJWT(jwt: string) {
    const id = this.app?.get('natadecoco_id');
    const secret = this.app?.get('natadecoco_secret');
    const client = await Hub.init({
      id: id,
      secret: secret,
      test: this.app?.get('node_env') === 'stg'
    });
    return client.VerifyJWT(jwt, ServiceType.AppHubService);
  }

  async authenticate(data: AuthenticationResult, params: Params) {
    const { tokenField } = this.configuration;
    const token = data[tokenField];
    const verifyResult =
      this.app?.get('exec_env') === 'global'
        ? await this.verifyJWT(token)
        : { issuer: 'did:ethr:0xfdd75875ab1bb0748e1e6cd20e4b3dd50f7fb7da' };
    let printCount;
    let printId;
    let printTotal;
    await recreate();
    const result = await this.app?.service('printcounts').find({
      query: {
        filter: {
          did: {
            _eq: verifyResult.issuer
          }
        }
      }
    });
    if (result.length !== 0) {
      console.log('printcounts', { result });
      const countData = result[0];
      printId = countData.id;
      const updatedOn = countData.dateUpdated;
      const isBeforeToday = moment(updatedOn).isBefore(moment(), 'day');
      printTotal = result[0].total;
      if (isBeforeToday) {
        this.app?.service('printcounts').update(countData.id, { value: 0 });
        printCount = 0;
      } else {
        printCount = result[0].value;
      }
    } else {
      const account = await this.app?.service('printcounts').create({ did: verifyResult.issuer, value: 0, total: 0 });
      printCount = 0;
      printId = account.id;
      printTotal = 0;
    }
    return {
      authentication: { strategy: this.name },
      user: {
        id: verifyResult.issuer,
        printCount,
        printId,
        printTotal
      },
      app: {
        id: this.app?.get('natadecoco_id')
      },
      env: this.app?.get('node_env')
    };
  }
}
