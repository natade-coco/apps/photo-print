import { Application } from '@feathersjs/feathers';
import { ServiceTypes } from './declarations';
import moment from 'moment';

const TIME_INTERVAL = 1000 * 60 * 60; // 1 hour

export const interval = async (app: Application<ServiceTypes>) => {
  setInterval(async () => {
    const result = await app.service('printdocs').find({
      query: {
        filter: {
          state: {
            _eq: 'published'
          }
        }
      }
    });
    const items = result.data;
    console.log(moment().utc(true).format('YYYY/MM/DD HH:mm'));
    try {
      items.map((item: any) => {
        if (!!moment().utc(true).diff(moment(item.date_created), 'days')) {
          app.service('files').remove(item.doc);
          app.service('printdocs').remove(item.id);
          console.log(`delete ${item.id}`);
        }
      });
    } catch (err) {
      console.log('datarefresh', { err });
      throw err;
    }
  }, TIME_INTERVAL);
};
